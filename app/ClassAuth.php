<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClassAkun;
use Carbon;
use App\FungsiGlobal;
use App\Model\Role\UserRole;
use App\VarGlobal;

class ClassAuth extends Model
{
    public function handleLogin($akun) {
    	$cek = UserRole::where("id_user",$akun->id)->first();
        if($cek==null) {
            $tambah = new UserRole;
                $tambah->id_user = $akun->id;
                $tambah->id_role = VarGlobal::$guest;
            return $tambah->save();
        }
        return true;
    }
}
