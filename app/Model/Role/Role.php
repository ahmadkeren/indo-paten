<?php

namespace App\Model\Role;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "role";
    protected $primaryKey = "id";

    public function user_role() {
    	return $this->hasMany("App\Model\Role\UserRole","id_role","id");
    }

    public function role_menu() {
    	return $this->hasMany("App\Model\Role\RoleMenu","id_role","id");
    }
}
