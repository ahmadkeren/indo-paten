<?php

namespace App\Model\Role;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = "role_menu";

    public function menu_child() {
    	return $this->belongsTo("App\Model\Menu\MenuChild","id_menu","id_menu");
    }
}
