<?php

namespace App\Model\Role;

use Illuminate\Database\Eloquent\Model;

class RoleAPI extends Model
{
    protected $table = "role_api";
}
