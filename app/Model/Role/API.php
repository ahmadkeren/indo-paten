<?php

namespace App\Model\Role;

use Illuminate\Database\Eloquent\Model;

class API extends Model
{
    protected $table = "api";
    protected $primaryKey = "id";

    public function role_api() {
    	return $this->hasMany("App\Model\Role\RoleAPI","id_api");
    }
}
