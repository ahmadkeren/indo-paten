<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Auth;

class login_logs extends Model
{
    protected $table   = "login_logs";
    protected $primaryKey = "id_log_login";
    public $timestamps = false;

    public function user() {
    	return $this->belongsTo("App\Model\User\User","id_user","id");
    }

    public function add_logs() {
    	$tambah = new login_logs;
    		$tambah->id_user = Auth::user()->id;
    	return $tambah->save();
    }
}
