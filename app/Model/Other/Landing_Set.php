<?php

namespace App\Model\Other;

use Illuminate\Database\Eloquent\Model;

class Landing_Set extends Model
{
    protected $table = "landing";
    protected $primaryKey = "id_landing";
}
