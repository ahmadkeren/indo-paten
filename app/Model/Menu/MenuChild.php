<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuChild extends Model
{
    protected $table = "menu_child";
    protected $primaryKey = "id_menu";

    public function menu_parent() {
    	return $this->belongsTo("App\Model\Menu\MenuParent","parent_id","id_parent");
    }

    public function role_menu() {
    	return $this->hasMany("App\Model\Role\RoleMenu","id_menu","id_menu");
    }
}
