<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuParent extends Model
{
    protected $table = "menu_parent";
    protected $primaryKey = "id_parent";

    public function menu_header() {
    	return $this->belongsTo("App\Model\Menu\MenuHeader","id_header","id_header");
    }

    public function menu_child() {
    	return $this->hasMany("App\Model\Menu\MenuChild","parent_id","id_parent");
    }
}
