<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class jenis_permohonan extends Model
{
    protected $table = "jenis_permohonan";
    protected $primaryKey = "id_jenis_permohonan";
    public $timestamps = false;

    public function set_harga() {
    	return $this->hasMany('App\Model\Master\set_harga',"id_jenis_permohonan","id_jenis_permohonan");
    }
}
