<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class data_kelas extends Model
{
    protected $table = "data_kelas";
    protected $primaryKey = "id_data_kelas";
    public $timestamps = false;

    public function permohonan() {
    	return $this->belongsTo('App\Model\Master\permohonan',"id_permohonan","id_permohonan");
    }

    public function kelas_master() {
    	return $this->belongsTo('App\Model\Master\kelas_master',"id_kelas","id_kelas");
    }
}
