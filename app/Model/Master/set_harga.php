<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class set_harga extends Model
{
    protected $table = "set_harga";
    protected $primaryKey = "id_set_harga";
    public $timestamps = false;

    public function jenis_pemohon() {
    	return $this->belongsTo('App\Model\Master\jenis_pemohon',"id_jenis_ref","id_jenis_pemohon");
    }

    public function jenis_permohonan() {
    	return $this->belongsTo('App\Model\Master\jenis_permohonan',"id_jenis_permohonan","id_jenis_permohonan");
    }
}
