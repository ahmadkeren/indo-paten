<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class kritik_saran extends Model
{
    protected $table = "kritik_saran";
    protected $primaryKey = "id_kritik_saran";
    public $timestamps = false;
}
