<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class jenis_pemohon extends Model
{
    protected $table = "jenis_pemohon";
    protected $primaryKey = "id_jenis_pemohon";
    public $timestamps = false;

    public function data_pemohon() {
    	return $this->hasMany('App\Model\Master\data_pemohon',"id_jenis_pemohon","id_jenis_pemohon");
    }

    public function syarat_berkas() {
    	return $this->hasMany('App\Model\Master\syarat_berkas',"id_jenis_pemohon","id_jenis_pemohon");
    }
}
