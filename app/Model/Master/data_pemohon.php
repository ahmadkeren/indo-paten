<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class data_pemohon extends Model
{
    protected $table = "data_pemohon";
    protected $primaryKey = "id_data_pemohon";
    public $timestamps = false;

    public function permohonan() {
    	return $this->belongsTo('App\Model\Master\permohonan',"id_permohonan","id_permohonan");
    }

    public function jenis_pemohon() {
    	return $this->belongsTo('App\Model\Master\jenis_pemohon',"id_jenis_pemohon","id_jenis_pemohon");
    }

    public function kewarganegaraan() {
        return $this->belongsTo('App\Model\Master\negara',"id_kewarganegaraan","id");
    }

    public function kota() {
    	return $this->belongsTo('App\Model\Master\kota',"id_kota","id");
    }

    public function kota_other() {
    	return $this->belongsTo('App\Model\Master\kota',"id_kota_other","id");
    }
}
