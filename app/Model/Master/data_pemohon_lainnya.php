<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class data_pemohon_lainnya extends Model
{
    protected $table = "data_pemohon_lainnya";
    protected $primaryKey = "id_data_pemohon_lainnya";
    public $timestamps = false;

    public function permohonan() {
    	return $this->belongsTo('App\Model\Master\permohonan',"id_permohonan","id_permohonan");
    }
}
