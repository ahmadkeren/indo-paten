<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class data_berkas extends Model
{
    protected $table = "data_berkas";
    protected $primaryKey = "id_data_berkas";
    public $timestamps = false;

    public function permohonan() {
    	return $this->belongsTo('App\Model\Master\permohonan',"id_permohonan","id_permohonan");
    }

    public function syarat_berkas() {
    	return $this->belongsTo('App\Model\Master\syarat_berkas',"id_syarat_berkas","id_syarat_berkas");
    }
}
