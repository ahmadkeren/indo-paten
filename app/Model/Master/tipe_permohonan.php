<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class tipe_permohonan extends Model
{
    protected $table = "tipe_permohonan";
    protected $primaryKey = "id_tipe_permohonan";
    public $timestamps = false;

    public function data_merek() {
    	return $this->hasMany('App\Model\Master\data_merek',"id_tipe_permohonan","id_tipe_permohonan");
    }
}
