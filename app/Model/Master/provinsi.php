<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
    protected $table = "provinsi";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function kota() {
    	return $this->hasMany('App\Model\Master\kota',"state_id","id");
    }

    public function negara() {
    	return $this->belongsTo('App\Model\Master\negara',"country_id","id");
    }
}
