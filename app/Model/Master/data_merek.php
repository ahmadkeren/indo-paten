<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class data_merek extends Model
{
    protected $table = "data_merek";
    protected $primaryKey = "id_data_merek";
    public $timestamps = false;

    public function permohonan() {
    	return $this->belongsTo('App\Model\Master\permohonan',"id_permohonan","id_permohonan");
    }

    public function tipe_merek() {
    	return $this->belongsTo('App\Model\Master\tipe_merek',"id_tipe_merek","id_tipe_merek");
    }

    public function tipe_permohonan() {
    	return $this->belongsTo('App\Model\Master\tipe_permohonan',"id_tipe_permohonan","id_tipe_permohonan");
    }
}
