<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\customer_message as CM;
use App\User;

class KotakMasukController extends \App\Http\Controllers\DefaultController
{
    public function respon_kontak() {
    	return view("cpanel.kotak_masuk.respon_kontak");
    }

    public function customer_message($id=null) {
    	if($id==null)
    		return view("cpanel.kotak_masuk.customer_message");
    	else
    		return $this->detail_customer_message($id);
    }

    private function detail_customer_message($id=null) {
    	$user = User::where("id",$id)->first();
    	if(!$user) return redirect(route('access_invalid'));
    	$data = CM::where("id_customer",$id)
                    ->orderBy("id_chat","DESC")
                    ->paginate(7);
        $has_read = CM::where("id_customer",$id)
                    ->where("is_answer",0)
                    ->update([
                        "has_read" => true
                    ]);
    	return view("cpanel.kotak_masuk.customer_message_detail")
    		->with("user",$user)
    		->with("data",$data);
    }
}
