<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Model\Other\Landing_Set;
use App\Model\Master\set_harga;

class PengaturanController extends \App\Http\Controllers\DefaultController
{
    public function user_role() {
    	return view("cpanel.pengaturan.user_role");
    }

    public function role_menu() {
    	return view("cpanel.pengaturan.role_menu")
    		->with("role", $this->getRoles());
    }

    public function role_api() {
    	return view("cpanel.pengaturan.role_api")
    		->with("role", $this->getRoles());
    }

    public function getRoles() {
    	return \App\Model\Role\Role::orderBy("id","ASC")->get();
    }

    public function landing_set() {
        $data = Landing_Set::orderBy("id_landing","DESC")->first();
        return view("cpanel.pengaturan.landing")
            ->with("data",$data);
    }

    public function konfig_harga() {
        $data = set_harga::with("jenis_permohonan")->get();
        return view("cpanel.pengaturan.harga")
            ->with("data",$data);
    }
}
