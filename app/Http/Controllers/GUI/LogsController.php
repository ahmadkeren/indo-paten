<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User\login_logs;

class LogsController extends \App\Http\Controllers\DefaultController
{
    public function login() {
    	$data = login_logs::with("user")->orderBy("login_at","DESC")->get();
    	return view("cpanel.logs.login")
    		->with("data",$data);
    }
}
