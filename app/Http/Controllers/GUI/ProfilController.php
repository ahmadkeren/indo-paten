<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;

class ProfilController extends \App\Http\Controllers\DefaultController
{
    public function index() {
    	return view("cpanel.profil.edit");
    }

    public function password() {
    	return view("cpanel.profil.password");
    }
}
