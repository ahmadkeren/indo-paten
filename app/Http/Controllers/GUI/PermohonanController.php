<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\status_permohonan;
use App\VarGlobal;

class PermohonanController extends \App\Http\Controllers\DefaultController
{
	public function permohonan_pending() {
		$status_permohonan = status_permohonan::where("selectable",true)->get();
		return view("cpanel.permohonan.pending")
			->with("status_permohonan",$status_permohonan);	
    }

    public function permohonan_masuk() {
    	return view("cpanel.permohonan.masuk")
    		->with("status_permohonan",VarGlobal::$sedang_diproses); // btn action
    }

    public function permohonan_diproses() {
        $selesaikan_permohonan = VarGlobal::$permohonan_selesai;
        $batalkan_permohonan = VarGlobal::$permohonan_dibatalkan;
        $status_permohonan = status_permohonan::where("selectable",true)->get();
        return view("cpanel.permohonan.diproses")
            ->with("selesaikan_permohonan",$selesaikan_permohonan)
            ->with("batalkan_permohonan",$batalkan_permohonan)
            ->with("status_permohonan",$status_permohonan);
    }

    public function permohonan_berhasil() {
        $status_permohonan = status_permohonan::where("selectable",true)->get();
        return view("cpanel.permohonan.berhasil")
            ->with("status_permohonan",$status_permohonan);
    }

    public function permohonan_dibatalkan() {
        $status_permohonan = status_permohonan::where("selectable",true)->get();
        return view("cpanel.permohonan.dibatalkan")
            ->with("status_permohonan",$status_permohonan);
    }

    public function permohonan_revisi() {
        $status_permohonan = status_permohonan::where("selectable",true)->get();
        return view("cpanel.permohonan.revisi")
            ->with("status_permohonan",$status_permohonan);
    }
}
