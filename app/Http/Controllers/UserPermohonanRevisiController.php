<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\negara;
use App\Model\Master\tipe_permohonan;
use App\Model\Master\tipe_merek;
use App\Model\Master\jenis_pemohon;
use App\Model\Master\kelas_master;
use App\Model\Master\permohonan;
use App\Model\Master\data_pemohon;
use App\Model\Master\data_pemohon_lainnya;
use App\Model\Master\data_merek;
use App\Model\Master\data_berkas;
use App\Model\Master\data_kelas;
use Auth;
use App\VarGlobal;
use App\User;

class UserPermohonanRevisiController extends UserPermohonanController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $negara = negara::all();
        $tipe_permohonan = tipe_permohonan::all();
        $tipe_merek = tipe_merek::all();
        $jenis_pemohon = jenis_pemohon::all();
        $kelas = kelas_master::select("kelas")->groupBy("kelas")->get();
        return view("page.user.permohonan.revisi")
            ->with("negara",$negara)
            ->with("tipe_permohonan",$tipe_permohonan)
            ->with("tipe_merek",$tipe_merek)
            ->with("jenis_pemohon",$jenis_pemohon)
            ->with("kelas",$kelas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_pemohon = Auth::user()->id;
        $tanggal_pengajuan = date("Y-m-d H:i:s");
        // Validasi jumlah kredit
        $jumlah_credit = Auth::user()->credit;
        $need_credit   = $request->jumlah_kelas * VarGlobal::$harga_kelas_on_credit;
        if($jumlah_credit < $need_credit)
            return [
                "success" => false,
                "pesan"   => "Jumlah kredit Anda tidak mencukupi untuk mengajukan permohonan revisi ini!",
            ];
        // End validasi jumlah kredit
        $tambah = new permohonan;
            $tambah->id_pemohon = $id_pemohon;
            $tambah->tanggal_pengajuan = $tanggal_pengajuan;
            $tambah->jumlah_kelas = $request->jumlah_kelas;
            $tambah->unknown_class = $request->unknown_class;
            $tambah->is_revisi  = true;
            $tambah->pembayaran_dikonfirmasi = true;
            $tambah->id_status_permohonan = VarGlobal::$sedang_diproses;
        if(!$tambah->save())
            return [
                "success" => false,
                "pesan"   => "Data gagal diproses!",
            ];
        $permohonan = permohonan::where("id_pemohon",$id_pemohon)
                        ->where("tanggal_pengajuan",$tanggal_pengajuan)
                        ->first();
        $id_permohonan = $permohonan->id_permohonan;
        // Handle for fix some error (only on some OS)
        if(
            $request->has("kota_other") &&
            $request->kota_other!="" &&
            $request->kota_other!="null" &&
            $request->kota_other!=null
        )
            $kota_other = (int) $request->kota_other;
        else
            $kota_other = null;
        // Simpan data pemohon
        $data_pemohon = new data_pemohon;
            $data_pemohon->id_permohonan = $id_permohonan;
            $data_pemohon->no_ktp = $request->no_ktp;
            $data_pemohon->nama_pemohon = $request->nama_pemohon;
            $data_pemohon->id_jenis_pemohon = $request->jenis_pemohon;
            $data_pemohon->id_kewarganegaraan = $request->kewarganegaraan;
            $data_pemohon->id_kota = $request->kota;
            $data_pemohon->alamat = $request->alamat;
            $data_pemohon->kode_pos = $request->kode_pos;
            $data_pemohon->no_hp = $request->nomor_hp;
            $data_pemohon->email = $request->email;
            $data_pemohon->no_whatsapp = $request->whatsapp;
            $data_pemohon->id_kota_other = $kota_other;
            $data_pemohon->alamat_other = $request->alamat_other;
            $data_pemohon->kode_pos_other = $request->kode_pos_other;
            $data_pemohon->no_hp_other = $request->nomor_hp_other;
            $data_pemohon->email_other = $request->email_other;
        $data_pemohon->save();
        // Simpan data (loop perlu)
        if($request->has("pemohon_tambahan") && $request->pemohon_tambahan!=null) {
            $data = $request->pemohon_tambahan;
            foreach ($data as $key => $value) {
                $data_pemohon_lainnya = new data_pemohon_lainnya;
                    $data_pemohon_lainnya->id_permohonan = $id_permohonan;
                    $data_pemohon_lainnya->no_ktp = str_replace("'", "", $key);
                    $data_pemohon_lainnya->nama_pemohon = $value;
                $data_pemohon_lainnya->save(); 
            }
        }
        // Simpan data merek
        $data_merek = new data_merek;
            $data_merek->id_permohonan = $id_permohonan;
            $data_merek->id_tipe_permohonan = $request->tipe_permohonan;
            $data_merek->id_tipe_merek = $request->tipe_merek;
            $data_merek->label_merek = $this->upload_label_merek($request->file('label_merek')); // gambar
            $data_merek->merek = $request->nama_merek;
            $data_merek->unsur_warna_label_merek = $request->unsur_warna;
        $data_merek->save();
        // Simpan data kelas (perlu loop)
        if($request->has("pilihan_kelas") && $request->pilihan_kelas!=null) {
            $data = $request->pilihan_kelas;
            foreach ($data as $key => $value) {
                $data_kelas = new data_kelas;
                    $data_kelas->id_permohonan = $id_permohonan;
                    $data_kelas->id_kelas = $value;
                $data_kelas->save();
            }
        }
        // Simpan data berkas (perlu loop)
        if($request->syarat_berkas != null) {
            $data = $request->syarat_berkas;
            foreach ($data as $key => $value) {
                $data_berkas = new data_berkas;
                    $data_berkas->id_permohonan = $id_permohonan;
                    $data_berkas->id_syarat_berkas = $key;
                    $data_berkas->berkas = $this->upload_berkas_syarat($request->file('syarat_berkas')[$key]); // file
                $data_berkas->save();
            }
        }
        // Kurangi credit
        $bayar = User::where("id",Auth::user()->id)->update([
            "credit" => Auth::user()->credit - $need_credit,
        ]);

        return [
           "success" => true,
           "pesan"   => "Permohonan berhasil disubmit!",
        ];   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
