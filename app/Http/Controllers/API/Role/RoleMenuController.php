<?php

namespace App\Http\Controllers\API\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role\Role;
use App\Model\Role\RoleMenu;
use App\Model\Menu\MenuChild;

class RoleMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(
            $request->has("id_role") &&
            $request->has("menus")
        ) {
            $id_role = $request->id_role;
            $menus   = $request->menus;
            if($this->roleValid($id_role)) {
                $hapus = RoleMenu::where("id_role",$id_role)->delete();
                foreach ($menus as $key => $value) {
                    $tambah = new RoleMenu;
                        $tambah->id_role = $id_role;
                        $tambah->id_menu = $value;
                    $tambah->save();
                }
                return [
                    "success" => true,
                    "pesan"   => "Role menu berhasil diperbaharui!",
                ];
            } else
                return [
                    "success" => false,
                    "pesan"   => "Role tidak terdaftar!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Perubahan role menu tidak dapat ditetapkan!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if($id==null)
            return [
                "success" => false,
                "pesan"   => "Role tidak dikenal!",
            ];
        if(!$this->roleValid($id))
            return [
                    "success" => false,
                    "pesan"   => "Role tidak terdaftar!",
                ];
        $list_menu = MenuChild::with("menu_parent")->get();
        $role_menu = RoleMenu::select("id_menu")->where("id_role",$id)->get();
        foreach ($list_menu as $keyParent => $valueParent) {
            $list_menu[$keyParent]["haveAccess"] = false;
            foreach ($role_menu as $keyChild => $valueChild) {
                if($valueParent->id_menu==$valueChild->id_menu) {
                    $list_menu[$keyParent]["haveAccess"] = true;
                    break;
                }
            }
        }
        return [
            "success" => true,
            "pesan"   => "Daftar menu berhasil diambil!",
            "data"    => $list_menu,
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function roleValid($id_role) {
        $cek = Role::where("id",$id_role)->first();
        if($cek) return true; else return false;
    }
}
