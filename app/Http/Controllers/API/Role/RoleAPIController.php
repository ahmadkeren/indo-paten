<?php

namespace App\Http\Controllers\API\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role\API;
use App\Model\Role\RoleAPI;

class RoleAPIController extends RoleMenuController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if(
            $request->has("id_role") &&
            $request->has("APIs")
        ) {
            $id_role = $request->id_role;
            $APIs   = $request->APIs;
            if($this->roleValid($id_role)) {
                $hapus = RoleAPI::where("id_role",$id_role)->delete();
                foreach ($APIs as $key => $value) {
                    $tambah = new RoleAPI;
                        $tambah->id_role = $id_role;
                        $tambah->id_api = $value;
                    $tambah->save();
                }
                return [
                    "success" => true,
                    "pesan"   => "Role API berhasil diperbaharui!",
                ];
            } else
                return [
                    "success" => false,
                    "pesan"   => "Role tidak terdaftar!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Perubahan role API tidak dapat ditetapkan!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=0)
    {
        if(!$this->roleValid($id))
            return [
                "success" => false,
                "pesan"   => "Role tidak terdaftar!",
            ];
        $list_api = API::get();
        $role_api = RoleAPI::select("id_api")->where("id_role",$id)->get();
        foreach ($list_api as $key_parent => $value_parent) {
            $list_api[$key_parent]["haveAccess"] = false;
            foreach ($role_api as $key_child => $value_child) {
                if($value_child->id_api == $value_parent->id) {
                    $list_api[$key_parent]["haveAccess"] = true;
                    break;
                }
            }
        }
        return [
            "success" => true,
            "pesan"   => "Daftar API dari Role yang bersangkutan berhasil diambil!",
            "data"    => $list_api,
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
