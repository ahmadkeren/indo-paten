<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User\User;
use App\FungsiGlobal;
use App\VarGlobal;
use Image; // plugin comress gambar
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        return $this->show(FungsiGlobal::getToken($request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // smart update
        $myId   = FungsiGlobal::getToken($request);
        $update = false;
        if($request->has('password')) {
            return $this->ganti_password($request,$myId);
        }
        if($request->hasFile('photo')) {
            return $this->uploadFoto($request->photo,$myId);
        }
        if(
            $request->exists('ponsel') &&
            $request->exists('whatsapp') &&
            $request->exists('status')
        ) {
            $update = User::where('id',$myId)->update([
                'ponsel' => $request->ponsel,
                'whatsapp' => $request->whatsapp,
                'status' => $request->status,
            ]);
        }
        if($update)
            return [
                "success" => true,
                "pesan"   => "Update berhasil!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Update gagal dilakukan!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $search = User::where("id",$id)
                    ->with("user_role")
                    ->first();
        if($search) {
            // Add new Info -> for Android service
            $isAdminPengumuman = false;
            $isAdminInfoLoker  = false;
            $isSuperAdmin      = false;
            foreach ($search->user_role as $key => $value) {
                if($value->id_role==VarGlobal::$idRoleSuperAdmin)
                    $isSuperAdmin = true;
                else if($value->id_role==VarGlobal::$idRoleAdminLoker)
                    $isAdminInfoLoker = true;
                else if($value->id_role==VarGlobal::$idRoleAdminInfo)
                    $isAdminPengumuman = true;
            }
            // end of Android addons service
            $class = new User;
            return [
                "success" => true,
                "circle"  => $search,
                "siakad"  => $class->getDetailSiakad($id),
                "addons"  => [
                    "isSuperAdmin"      => $isSuperAdmin,
                    "isAdminPengumuman" => $isAdminPengumuman,
                    "isAdminInfoLoker"  => $isAdminInfoLoker,
                ]
            ];
        } else
            return [
                "success" => false,
                "pesan"   => "Profil pengguna tidak ditemukan!",
            ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadFoto($foto,$myId) {
        $format = $foto->getmimeType();
        $size   = $foto->getsize();

        $allowed = ["image/png","image/jpg","image/jpeg"];

        if(in_array($format, $allowed)) {;
            $newIMG = Image::make($foto);
            $newIMG->resize(150,150); // dibuat jadi kotak
            $update = User::where('id',$myId)->update([
                //'photo' => $newIMG->encode("jpg",80), // save pure blob
                'photo' => base64_encode($newIMG->encode("jpg",80)), // save has encoded
            ]);
            if($update)
                return [
                    "success" => true,
                    "code"    => "#S28",
                    "pesan"   => "Foto berhasil diunggah!",
                ];
            else
                return [
                    "success" => false,
                    "code"    => "#43",
                    "pesan"   => "Foto gagal diungagh!",
                ];
        } else
            return [
                "success" => false,
                "code"    => "#E42",
                "pesan"   => "Konten tidak didukung!",
            ];
    }

    public function ganti_password($request,$id) {
        $akun = User::where("id",$id)->first();
        if(!$akun)
            return [
                "success" => false,
                "pesan"   => "Akun tidak ditemukan!",
            ];
        if(!$request->has("password_current"))
            return [
                "success" => false,
                "pesan"   => "Kata sandi belum diinput!",
            ];
        if(!$request->has("password"))
            return [
                "success" => false,
                "pesan"   => "Kata sandi baru belum diinput!",
            ];
        if(!$request->has("password_confirm"))
            return [
                "success" => false,
                "pesan"   => "Konfirmasi kata sandi belum diinput!",
            ];
        $current  = $request->password_current;
        $password = $request->password;
        $confirm  = $request->password_confirm;
        if($password!=$confirm)
            return [
                "success" => false,
                "pesan"   => "Konfirmasi kata sandi tidak sama!",
            ];
        // sandi baru == konfirmasi sandi
        $db_password = $akun->password;
        // Cek password
        if (Hash::check($current,$db_password)) {
            // sandi valid
            $update = User::where("id",$id)
                        ->update([
                            "password" => Hash::make($password),
                        ]);
            if($update)
                return [
                    "success" => true,
                    "pesan"   => "Kata sandi berhasil diperbaharui!",
                ];
            else
                return [
                    "success" => false,
                    "pesan"   => "Kata sandi gagal diperbaharui!",
                ];
        } else
            return [
                "success" => false,
                "pesan" => "Kata sandi tidak valid!",
            ];
    }
}
