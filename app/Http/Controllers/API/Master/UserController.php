<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FungsiGlobal;
use App\Model\User\User;
use App\Model\Role\UserRole;
use Hash;
use App\VarGlobal;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->name))
            return [
                "success" => false,
                "pesan"   => "Nama dari pengguna belum diinput!",
            ];
        if(!isset($request->email))
            return [
                "success" => false,
                "pesan"   => "Email belum diinput!",
            ];
        if($request->email != trim($request->email))
            return [
                "success" => false,
                "pesan"   => "Email tidak boleh mengandung spasi!",
            ];
        if(!isset($request->password))
            return [
                "success" => false,
                "pesan"   => "Password belum diinput!",
            ];
        $cek = User::where("email",$request->email)->first();
        if($cek) return [
            "success" => false,
            "pesan"   => "Email telah digunakan!",
        ];
        $usertype = "user";
        $id_role  = VarGlobal::$user;
        if(
            $request->usertype=="karyawan" || 
            $request->usertype=="pegawai"
        ) {
            $usertype = "karyawan";
            $id_role  = VarGlobal::$karyawan;
        }
        $tambah = new User;
            $tambah->name = $request->name;
            $tambah->email = $request->email;
            $tambah->password = Hash::make($request->password);
            $tambah->ponsel = $request->ponsel;
            $tambah->usertype = $request->usertype;
        if($tambah->save()) {
            // tambah role pengguna
            $user = User::where("email",$request->email)->first();
            $tambah_role = new UserRole;
                $tambah_role->id_user = $user->id;
                $tambah_role->id_role = $id_role;
            $tambah_role->save();
            return [
                "success" => true,
                "pesan"   => ucfirst($usertype)." berhasil ditambahkan!",
            ];
        }
        else
            return [
                "success" => false,
                "pesan"   => ucfirst($usertype)." gagal ditambahkan!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Digunakan untuk fitur update data karyawan
        // Validasi email
        $email_exist = User::where("id","!=",$id)->where("email",$request->email)->first();
        if($email_exist)
            return [
                "success" => false,
                "pesan"   => "Email telah digunakan pada akun lain!",
            ];
        // Edit data karyawan
        $update = User::where("id",$id)->update([
            "name" => $request->name,
            "email" => $request->email,
            "ponsel" => $request->ponsel,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => "Data karyawan berhasil diperbaharui!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Data karyawan sudah (atau gagal) diperbaharui!",
            ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cari = User::where("id",$id)->first();
        if(!$cari)
            return [
                "success" => false,
                "pesan"   => "User yang akan dihapus tidak ditemukan!",
            ];
        if(!$cari->removable)
            return [
                "success" => false,
                "pesan"   => "User yang dimaksudkan tidak diperbolehkan untuk dihapus!",
            ];
        $usertype = $cari->usertype;
        $hapus = User::where("id",$cari->id)->delete();
        if($hapus)
            return [
                "success" => true,
                "pesan"   => ucfirst($usertype)." berhasil dihapus!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => ucfirst($usertype)." sudah (atau tidak dapat) dihapus!",
            ];
    }
}
