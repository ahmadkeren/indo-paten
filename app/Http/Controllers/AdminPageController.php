<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\Master\status_permohonan;

class AdminPageController extends Controller
{
    public function content_page($scope=null) {
    	if(!Auth::user()) return redirect(Route('home'));
    	switch ($scope) {
    		case "permohonan_masuk":
    			$status_permohonan = status_permohonan::all();
    			return view("page.admin.permohonan_masuk")
    				->with("status_permohonan",$status_permohonan);
    			break;
    		default:
                $auto_confirm = \App\FungsiGlobal::smart_handle_auto_confirm_payment();
    			return view("page.admin.welcome");
    			break;
    	}
    }
}
