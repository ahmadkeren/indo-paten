<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassMenu;
use Auth;
use View;

class DefaultController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(Auth::guest()) return redirect(Route('login_page'));
            
            $menu = new ClassMenu;
            View::share('menu', $menu->getMenu());

            return $next($request);
        });

    }
}
