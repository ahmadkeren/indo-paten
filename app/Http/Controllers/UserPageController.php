<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\Master\customer_message as CM;

class UserPageController extends Controller
{
    public function content_page($scope=null) {
    	if(!Auth::user()) return redirect(Route('home'));
    	switch ($scope) {
    		case 'billing':
                $this->auto_confirm();
    			return view("page.user.billing");
    			break;

    		case 'profil':
    			return view("page.user.profil");
    			break;

            case 'mail':
                $data = CM::where("id_customer",Auth::user()->id)
                            ->orderBy("id_chat","DESC")
                            ->paginate(7);
                $has_read = CM::where("id_customer",Auth::user()->id)
                            ->where("is_answer",1)
                            ->update([
                                "has_read" => true
                            ]);
                return view("page.user.mail")
                    ->with("data",$data);
                break;

    		default:
                $this->auto_confirm();
    			return view("page.user.permohonan_saya");
    			break;
    	}
    }

    private function auto_confirm() {
        return \App\FungsiGlobal::smart_handle_auto_confirm_payment(Auth::user()->id);
    }
}
