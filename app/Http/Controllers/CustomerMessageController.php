<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\customer_message as CM;
use Auth;

class CustomerMessageController extends Controller
{
    public function sendMessage(Request $request) {
    	$tambah = new CM;
    		$tambah->id_customer = Auth::user()->id;
    		$tambah->message = $request->message;
    		$tambah->created_by = Auth::user()->id;
    	$tambah->save();
    	return redirect()->back();
    }

    public function sendMessageAdmin($id=null, Request $request) {
    	if($id!=null) {
    		$tambah = new CM;
	    		$tambah->id_customer = $id;
	    		$tambah->is_answer = true;
	    		$tambah->message = $request->message;
	    		$tambah->created_by = Auth::user()->id;
	    	$tambah->save();
    	}
    	return redirect()->back();
    }
}
