<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has("current_password"))
            return [
                "success" => false,
                "pesan"   => "Akses tidak valid!",
            ];
        $email = $request->email;
        $ponsel = $request->ponsel;
        $current_password = $request->current_password;
        $new_password = $current_password;
        if($request->has("new_password") && $request->new_password!="" && $request->new_password!=null)
            $new_password = $request->new_password;
        if (Hash::check($current_password,Auth::user()->password)) {
            $email_exist = User::where("id","!=",Auth::user()->id)->where("email",$email)->first();
            if($email_exist)
                return [
                    "success" => false,
                    "pesan"   => "Email telah digunakan pada akun lain!",
                ];
            $update = User::where("id",Auth::user()->id)->update([
                "email" => $email,
                "ponsel" => $ponsel,
                "password" => Hash::make($new_password),
            ]);
            if($update)
                return [
                    "success" => true,
                    "pesan"   => "Profil berhasil diperbaharui!",
                ];
            else
                return [
                    "success" => false,
                    "pesan"   => "Profil sudah (atau gagal) diperbaharui!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Kata sandi yang dimasukkan salah!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
