<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FungsiGlobal;

class GlobalController extends Controller
{
    public function encrypt($id=null){
    	return FungsiGlobal::encrypt($id);
    }
}
