<?php

namespace App\Http\Middleware;

use Closure;
use App\FungsiGlobal;
use App\User;
use App\ClassAkun;

class AuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('token')==null)
            return response([
                    "success" => false,
                    "code"    => "#E00",
                    "pesan"   => "Silahkan login terlebih dahulu!",
                ]);
        if(FungsiGlobal::getToken($request)!=false) {
            // cek apakah akun masih ada di database
            if(ClassAkun::select('id')->where('id',FungsiGlobal::getToken($request))->first())
                return $next($request);
            else
                return response([
                    "success" => false,
                    "code"    => "#E00",
                    "pesan"   => "Token tidak terdaftar!",
                ]);
        } else
            return response([
                "success" => false,
                "code"    => "#E00",
                "pesan"   => "Token tidak valid!",
            ]);
    }
}
