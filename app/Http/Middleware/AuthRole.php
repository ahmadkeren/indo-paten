<?php

namespace App\Http\Middleware;

use Closure;
use App\FungsiGlobal;
use App\ClassRole;
use Illuminate\Support\Facades\Route;
use App\Model\Role\API;

class AuthRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $canAccess = false;
        // get current RuleName
        $routeName = Route::currentRouteName();
        // get rule can access
        $ruleCanAccess = API::where("routeName",$routeName)->first();
        if($ruleCanAccess==null)
            $canAccess = true;
        else {
            $ruleCanAccess = $ruleCanAccess->role_api;
            // get idUser
            $idUser = FungsiGlobal::getToken($request);
            // get myRole
            $role   = new ClassRole;
            $myRole = $role->getMyRole($idUser);
            // validasi Role
            foreach ($ruleCanAccess as $keyOne => $valueOne) {
                foreach ($myRole as $keyTwo => $valueTwo) {
                    if($valueOne->id_role==$valueTwo->id_role) {
                        $canAccess = true; break;
                    }
                }
                if($canAccess==true) break;
            }
            /*return response([ // debuging
                "ruleCanAccess" => $ruleCanAccess,
                "myRole" => $myRole,
            ]);*/
        };
        if($canAccess==true)
            return $next($request);
        else
            return response([
                "success" => false,
                "code"    => "#E19",
                "pesan"   => "Anda tidak memiliki akses!",
            ]);
    }
}
