<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VarGlobal extends Model
{
	//List id role
	public static $super_admin = 1;
	public static $admin = 2;
	public static $user = 3;
	public static $karyawan = 4;
	//List admin
	public static $array_admin = [1,2];
	public static $array_karyawan = [4];
	//Sate permohonan
	public static $belum_diproses = 1;
	public static $sedang_diproses = 2;
	public static $permohonan_selesai = 3; //permohonan berhasil
	public static $permohonan_dibatalkan = 4;
	// Credit
	public static $harga_kelas_on_credit = 1; // 1 kelas = 1 kredit
	//List state pembayaran di gateway
	public static $array_belum_dibayar = ['PENDING','EXPIRED'];
    public static $array_dibayar = ['PAID','SETTLED'];
    //Harga permohonan
    public static $id_merek_baru_umum = 1;
    public static $id_merek_baru_umkm = 2;
    public static $id_merek_panjang_umum= 3;
    public static $id_merek_panjang_umkm = 4;
    public static $id_merek_inter = 5;
}
