<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\Role\UserRole;
use App\Model\Role\RoleMenu;
use App\Model\Menu\MenuHeader;
use App\Model\Menu\MenuParent;
use App\Model\Menu\MenuChild;
use Auth;

class ClassMenu extends Model
{
    public function getMenu(){
    	$role_menu   = RoleMenu::select("id_menu")
    				->whereIn("id_role",
    					UserRole::select("id_role")
    					->where("id_user",Auth::user()->id)
    					->get()
    				)
    				->groupBy("id_menu") // anti redudansi menu
    				->get();
    	$menu_header = MenuHeader::with("menu_parent")->get();
    	// generate menu
    	foreach ($menu_header as $key_header => $value_header) {
    		foreach ($value_header->menu_parent as $key_parent => $value_parent) {
				$menu_child = MenuChild::whereIn("id_menu",$role_menu)
							->where("parent_id",$value_parent->id_parent)
                            ->where("visible",true)
							->get();
				$menu_header[$key_header]->menu_parent[$key_parent]["menu_child"] = $menu_child;
    		}
    	}
    	return $this->fixHeader($this->fixParent($menu_header));
    	//return $this->fixParent($menu_header);
    	//return MenuHeader::with("menu_parent.menu_child")->get(); // return all menu
    }

    public function fixParent($menu) {
    	// hapus parent menu yang tidak memiliki child menu
    	foreach ($menu as $key_header => $value_header) {
    		foreach ($value_header->menu_parent as $key_parent => $value_parent) {
    			if(count($value_parent->menu_child)==0)
    				unset($menu[$key_header]->menu_parent[$key_parent]);
    		}
    	}
    	// reindex array
    	$new_menu = [];
    	foreach ($menu as $key_header => $value_header) {
    		$menu_value = [];
	    		$menu_value['id_parent'] = $value_header->id_header;
	    		$menu_value['header_name'] = $value_header->header_name;
	    		$menu_value['menu_parent'] = $value_header->menu_parent->values();
    		array_push($new_menu, (object) $menu_value);
    	}
    	return $new_menu;
    }

    public function fixHeader($menu) {
    	foreach ($menu as $key_header => $value_header) {
			if(count($value_header->menu_parent)==0)
				unset($menu[$key_header]);
    	}
    	return array_values($menu);
    }
}
