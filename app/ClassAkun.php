<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassAkun extends Model
{
    protected $connection = 'mysql'; // database utama
    protected $table      = 'users';
    protected $primaryKey = 'id';
    
    public function user_role() {
        return $this->hasMany("App\Model\Role\UserRole","id_user","id");
    }

    public function unread_customer_message_count() {
    	$count = \App\Model\Master\customer_message::where("id_customer",$this->id)
                ->where("is_answer",true)
                ->where("has_read",false)
                ->count();
        return $count;
    }
}
