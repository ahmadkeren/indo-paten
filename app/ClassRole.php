<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\Role\Role;
use App\Model\Role\UserRole;
use App\VarGlobal;

class ClassRole extends Model
{
    public function addRole($idUser,$idRole) {
	    $newRole = new UserRole;
	    	$newRole->id_user = $idUser;
	    	$newRole->id_role = $idRole;
	    return $newRole->save();
    }

    public function handleZeroRule($idUser) {
        $oneRule = UserRole::where("id_user",$idUser)->first();
        if($oneRule) return true;
        return $this->addRole($idUser,VarGlobal::$user);
    }

    public function getMyRole($idUser) {
        if($this->handleZeroRule($idUser)) {
            $myRole = UserRole::where("id_user",$idUser)->with('role')->get();
            if($myRole)
                return $myRole;
            else
                return null;
        } else
            return null;
    }
}
