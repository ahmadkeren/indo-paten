// Konfigurasi Menu Collapse
$(document).on('click', '.menu-toggle, .modern-nav-toggle', function(e) {
    if(local_get_menu()=="collapse")
      local_set_menu("menu");
    else
      local_set_menu("collapse");
})

function local_set_menu($mode) {
  localStorage.setItem("menu",$mode);
}

function local_get_menu() {
  $local_set_menu = localStorage.getItem("menu");
  if($local_set_menu==null || $local_set_menu==undefined) {
    local_set_menu("normal");
  }
  return localStorage.getItem("menu")
}

function local_handle_menu() {
  if(
    local_get_menu()=="collapse" &&
    $('body').hasClass("menu-hide") == false
    ) $.app.menu.toggle();
}

// Konfigurasi dinamic search
$(".search-input input").change( function(e) {
    local_set_prev_search(this.value);
})

function local_set_prev_search($keyword) {
  localStorage.setItem("search",$keyword);
}

function local_get_prev_search() {
  $local_set_menu = localStorage.getItem("search");
  if($local_set_menu==null || $local_set_menu==undefined) {
    local_set_prev_search(null);
  }
  return localStorage.getItem("search")
}

function local_handle_prev_search() {
  $local_prev_search = local_get_prev_search();
  if($local_prev_search != null && $local_prev_search != undefined && $local_prev_search !="" && $local_prev_search != "null") {
    $(".search-input").addClass("open");
    $(".search-input input").val($local_prev_search);
  }
}

// Run Default Konfig
$(document).ready(function(){
  local_handle_menu();
  local_handle_prev_search();
})