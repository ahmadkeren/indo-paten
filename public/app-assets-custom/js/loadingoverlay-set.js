function showLoading() {
	$.LoadingOverlay("show", {
		imageResizeFactor: 0.5,
	});
}

function hideLoading() {
	$.LoadingOverlay("hide");
}

function miniLoading($lokasi) {
	$($lokasi).LoadingOverlay("show", {
		background  : "rgba(165, 190, 100, 0.5)"
	});
}

function hideMiniLoading($lokasi) {
	$($lokasi).LoadingOverlay("hide", true);
}