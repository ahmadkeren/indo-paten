function miniNotif($mode,$pesan) {
	$config = {"progressBar": true, timeOut: 3000};
	switch($mode) {
		case "success": toastr.success($pesan, "Selamat!", $config); break;
		case "info": toastr.info($pesan, "Informasi!", $config); break;
		case "warning": toastr.warning($pesan, "Warning!", $config); break;
		case "error": toastr.error($pesan, "Oops!", $config); break;
	}
}