function showLoading() {
	$.LoadingOverlay("show", {
	    size: 5,
	});
}

function hideLoading() {
	$.LoadingOverlay("hide");
}

function showToast($tipe=null,$pesan=null) {
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	switch($tipe) {
		case "success": toastr["success"]($pesan, "Success!"); break;
		case "warning": toastr["warning"]($pesan, "Warning!"); break;
		case "info": toastr["info"]($pesan, "Info!"); break;
		case "danger": toastr["error"]($pesan, "Oops!"); break;
		case "error": toastr["error"]($pesan, "Oops!"); break;
		default: toastr["info"]($pesan, "Information!"); break;
	}
}

function handle_select($string,$field) {
		if($string=="" || $string==null || $string==NaN) {
			showToast("warning",$field+" belum dipilih!");
			return false;
		} else
			return true;
}

function handle_require($string,$field) {
	if($string=="" || $string.trim().length==0) {
		showToast("warning",$field+" belum diinput!");
		return false;
	} else
		return true;
}

function handle_checked($id_check,$field) {
	$checked = $($id_check).is(":checked");
	if(!$checked) {
		showToast("warning",$field+" belum diceklis!");
		return false;
	} else
		return true;
}

function handle_char($string,$field,$char_length) {
	if($string=="" || $string.length<$char_length) {
		showToast("warning",$field+" setidaknya terdiri dari "+$char_length+" karakter!");
		return false;
	}
		return true;
}

function handle_length($string,$length,$field) {
	if($string.length<$length || $string.trim().length==0) {
		showToast("warning",$field+" setidaknya terdiri dari "+$length+" karakter!");
		return false;
	} else
		return true;
}

function redirect($link,$time) {
	if($time=="") $time = 100;
	setTimeout(function(){
	    window.open($link,"_SELF");
	},$time);
}

function newTab($link,$time) {
	if($time=="") $time = 100;
	setTimeout(function(){
	    window.open($link,"_BLANK");
	},$time);
}