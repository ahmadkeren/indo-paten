<script type="text/javascript">
	function toogle_jenis_pemohon() {
		$jenis = $("input[name=jenis_pemohon]:checked").val();
		$("input[name=upload_berkas][value='"+$jenis+"']").prop("checked",true);
		handle_syarat_form($jenis);
	}

	function toogle_upload_syarat() {
		$syarat = $("input[name=upload_berkas]:checked").val();
		$("input[name=jenis_pemohon][value='"+$syarat+"']").prop("checked",true);
		handle_syarat_form($syarat);
	}

	function handle_syarat_form($id_jenis) {
		$(".block-syarat").attr("disabled","disabled");
		$(".syarat-"+$id_jenis).removeAttr("disabled");
		$(".block-syarat").val("");
	}

	$(document).ready(function(){
		toogle_upload_syarat();
	})
</script>