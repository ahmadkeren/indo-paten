<script type="text/javascript">
	function generate_html_location(res,scope) {
		$res = '<option value="" disabled="" selected="">--Pilih '+scope+'--</option>';
	    if(res.length>0) {
	    	for($i=0;$i<res.length;$i++) {
	          $res+='<option value="'+res[$i].id+'">'+res[$i].name+'</option>';
	        }
	    };
	    return $res;
	}
	function getProvinsi() {
		$id_negara = $("#negara").val();
		showLoading();
	    $.ajax({
	        url    :"{{Route('service')}}/getProvinsi/"+$id_negara,
	        success: function(res) {
	          $("#provinsi").html(generate_html_location(res,"Provinsi"));
	          $("#provinsi").removeAttr("disabled");
	          hideLoading();
	        },
	        error: function() {
	          hideLoading();
	          showToast("danger","Tidak dapat terhubung ke server, coba refresh laman.");
	        },
	    })
	}
	function getKota() {
		$id_provinsi = $("#provinsi").val();
		showLoading();
	    $.ajax({
	        url    :"{{Route('service')}}/getKota/"+$id_provinsi,
	        success: function(res) {
	          $("#kota").html(generate_html_location(res,"Kabupaten/Kota"));
	          $("#kota").removeAttr("disabled");
	          hideLoading();
	        },
	        error: function() {
	          hideLoading();
	          showToast("danger","Tidak dapat terhubung ke server, coba refresh laman.");
	        },
	    })
	}
	function getProvinsiScope($get,$set) {
		$id_negara = $("#"+$get).val();
		showLoading();
	    $.ajax({
	        url    :"{{Route('service')}}/getProvinsi/"+$id_negara,
	        success: function(res) {
	          $("#"+$set).html(generate_html_location(res,"Provinsi"));
	          $("#"+$set).removeAttr("disabled");
	          hideLoading();
	        },
	        error: function() {
	          hideLoading();
	          showToast("danger","Tidak dapat terhubung ke server, coba refresh laman.");
	        },
	    })
	}
	function getKotaScope($get,$set) {
		$id_provinsi = $("#"+$get).val();
		showLoading();
	    $.ajax({
	        url    :"{{Route('service')}}/getKota/"+$id_provinsi,
	        success: function(res) {
	          $("#"+$set).html(generate_html_location(res,"Kabupaten/Kota"));
	          $("#"+$set).removeAttr("disabled");
	          hideLoading();
	        },
	        error: function() {
	          hideLoading();
	          showToast("danger","Tidak dapat terhubung ke server, coba refresh laman.");
	        },
	    })
	}
	function other_addess_toggle() {
		$checked = $("#another_address").is(":checked");
		if($checked) {
			$(".other_address").removeAttr("disabled");
		} else {
			$(".other_address").attr("disabled","disabled");
		}
	}
	$(document).ready(function(){
		getProvinsi();
		other_addess_toggle();
	})
</script>