<script type="text/javascript">
	function other_person_toggle() {
		$checked = $("#another_pemohon").is(":checked");
		if($checked) {
			$(".other_pemohon").removeAttr("disabled");
		} else {
			$(".other_pemohon").attr("disabled","disabled");
			$("#pemohon_tambahan").DataTable()
		        .clear()
		        .draw();
		}
	}

	$(document).ready(function(){
		other_person_toggle();
	})

	$btn_hapus_pemohon_tambahan = "<button class='btn btn-sm btn-danger hapus' type='button'><i lass='fa fa-times'></i> Hapus</button>";


	function tambah_pemohon_tambahan() {
		$no_ktp_tmp = $("#ktp_permohon_tambahan").val();
		$nama_pemohon_tmp = $("#nama_permohon_tambahan").val();
		if(!handle_require($no_ktp_tmp,"Nomor KTP pemohon tambahan")) return;
		if(!handle_length($no_ktp_tmp,15,"Nomor KTP pemohon tambahan")) return;
		if(!handle_require($nama_pemohon_tmp,"Nama pemohon tambahan")) return;
		if(!handle_length($nama_pemohon_tmp,3,"Nama pemohon tambahan")) return;
		$("#pemohon_tambahan").DataTable().row.add(
	        [$no_ktp_tmp,$nama_pemohon_tmp,$btn_hapus_pemohon_tambahan],
	    ).draw();
	    $("#ktp_permohon_tambahan").val("");
	    $("#nama_permohon_tambahan").val("");
	}

	$('#pemohon_tambahan').on('click', 'button.hapus', function () {
	    $("#pemohon_tambahan").DataTable()
	        .row($(this).parents('tr'))
	        .remove()
	        .draw();
	});
</script>