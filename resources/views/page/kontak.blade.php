@extends("layouts.template")

@section("content")
<div class="row">
	<div class="col-md-12">
		<p class="bold uppercase center underline">Kontak Kami</p>
	</div>
</div>

<br>

<form>
	<div class="form-group row">
	    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Lengkap</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="nama_lengkap" placeholder="Masukkan Nama Lengkap" maxlength="30" required>
	    </div>
 	</div>
 	<div class="form-group row">
	    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat Email</label>
	    <div class="col-sm-10">
	      <input type="email" class="form-control" id="alamat_email" placeholder="Masukkan alamat email" maxlength="50" required>
	    </div>
 	</div>
 	<div class="form-group row">
	    <label for="inputPassword" class="col-sm-2 col-form-label">Pertanyaan / Pernyataan</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" id="pernyataan" placeholder="Masukkan pernyataan/pertanyaan Anda mengenai indopaten.com" rows="5"></textarea>
	    </div>
 	</div>
 	<div class="form-group row right">
	    <div class="col-sm-12">
	      <button class="btn btn-md btn-primary" type="button" onclick="submit_kritik()">Kirim</button>
	    </div>
 	</div>
</form>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
<script type="text/javascript">
	function submit_kritik() {
		$nama = $("#nama_lengkap").val();
		$email = $("#alamat_email").val();
		$pernyataan = $("#pernyataan").val();
		if(!handle_require($nama,"Nama")) return;
		if(!handle_require($email,"Alamat email")) return;
		if(!handle_require($pernyataan,"Pernyataan atau pertanyaan")) return;
		showLoading();
		$.ajax({
			url : "{{route('service')}}/sendKirtikSaran",
			method : "POST",
			data: {
				"nama" : $nama,
				"email" : $email,
				"pernyataan" : $pernyataan,
			},
			success: function(res) {
				hideLoading();
				if(res.success) {
					showToast("success",res.pesan);
					$("#nama_lengkap").val("");
					$("#alamat_email").val("");
					$("#pernyataan").val("");
				} else
					showToast("error",res.pesan);
			},
			error: function() {
				hideLoading();
				showToast("error","Tidak dapat terhubung ke server!");
			}
		})
	}
</script>
@endsection