@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold center underline">INVOICE - #INV{{$permohonan->id_permohonan}}</h4>
    </div>
</div>

<br>

<form action="#" method="POST" enctype="multipart/form-data" id="form_merek_baru" onsubmit="return false">
  {{csrf_field()}}
  <div class="row">
      <div class="col-md-10">
          <h6 class="bold">Data Pemohon</h6>
      </div>
      <div class="col-md-2 right">
        <span class="red">*) Wajib</span>
      </div>
  </div>
  <br>
   <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor KTP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_ktp" placeholder="-" maxlength="16" disabled="disabled" value="{{$permohonan->data_pemohon->no_ktp}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Pemohon<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama_pemohon" placeholder="Nama Pemohon" maxlength="30" required="required" disabled="disabled" value="{{$permohonan->data_pemohon->nama_pemohon}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kewarganegaraan<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="kewarganegaraan" required="required" disabled="disabled">
        <option value="" disabled="" selected="">{{$permohonan->data_pemohon->kewarganegaraan->name}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis Pemohon<span class="red">*</span></label>
    <div class="col-sm-10" id="jenis_pemohon">
      @foreach($jenis_pemohon as $val)
      @if($val->id_jenis_pemohon == $permohonan->data_pemohon->id_jenis_pemohon)
      <input type="radio" name="jenis_pemohon" value="{{$val->id_jenis_pemohon}}" required="required" onchange="toogle_jenis_pemohon()" disabled="disabled" checked="checked"> {{$val->jenis_pemohon}}
      @else
      <input type="radio" name="jenis_pemohon" value="{{$val->id_jenis_pemohon}}" required="required" onchange="toogle_jenis_pemohon()" disabled="disabled"> {{$val->jenis_pemohon}}
      @endif 
      &nbsp &nbsp
      @endforeach
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="negara" onchange="getProvinsi()" required="required" disabled="disabled">
      	<option value="" disabled="" selected="">{{$permohonan->data_pemohon->kota->provinsi->negara->name}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="provinsi" onchange="getKota()" required="required" disabled="disabled">
      	<option value="" disabled="" selected="" disabled="disabled">{{$permohonan->data_pemohon->kota->provinsi->name}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="kota" disabled="disabled" required="required" disabled="disabled">
      	<option value="" disabled="" selected="">{{$permohonan->data_pemohon->kota->name}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat<span class="red">*</span></label>
    <div class="col-sm-10">
    	<textarea class="form-control" placeholder="Alamat pemohon utama" rows="3" id="alamat" required="required" disabled="disabled">{{$permohonan->data_pemohon->alamat}}</textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="kode_pos" placeholder="Kode Pos" maxlength="8" disabled="disabled" value="{{$permohonan->data_pemohon->kode_pos}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP/Telp<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_hp" placeholder="Nomor HP/Telp" maxlength="15" required="required" disabled="disabled" value="{{$permohonan->data_pemohon->no_hp}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="Alamat Email" maxlength="30" required="required" disabled="disabled" value="{{$permohonan->data_pemohon->email}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">WhatsApp<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="whatsapp" placeholder="Nomor WhatsApp" maxlength="15" required="required" disabled="disabled" value="{{$permohonan->data_pemohon->no_whatsapp}}">
    </div>
  </div>
  <hr>
  <div class="row">
  	<div class="col-sm-12">
      @if($permohonan->data_pemohon->kota_other!=null)
  		<input type="checkbox" name="" id="another_address" onchange="other_addess_toggle()" checked="checked" disabled="disabled"> Jika alamat surat menyurat tidak sama dengan Identitas Pemohon<br><br>
      @else
      <input type="checkbox" name="" id="another_address" onchange="other_addess_toggle()" disabled="disabled"> Alamat surat menyurat tidak sama dengan Identitas Pemohon<br><br>
      @endif
  	</div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="negara_other" onchange="getProvinsiScope('negara_other','provinsi_other')" disabled="disabled">
        @if($permohonan->data_pemohon->kota_other==null)
        <option value="" disabled="" selected="">-</option>
        @else
        <option value="" disabled="" selected="">{{$permohonan->data_pemohon->kota_other->provinsi->negara->name}}</option>
        @endif
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="provinsi_other" onchange="getKotaScope('provinsi_other','kota_other')" disabled="disabled">
        @if($permohonan->data_pemohon->kota_other==null)
        <option value="" disabled="" selected="">-</option>
        @else
        <option value="" disabled="" selected="">{{$permohonan->data_pemohon->kota_other->provinsi->name}}</option>
        @endif
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="kota_other" disabled="disabled">
        @if($permohonan->data_pemohon->kota_other==null)
        <option value="" disabled="" selected="">-</option>
        @else
        <option value="" disabled="" selected="">{{$permohonan->data_pemohon->kota_other->name}}</option>
        @endif
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat</label>
    <div class="col-sm-10">
    	<textarea class="form-control other_address" placeholder="-" rows="3" id="alamat_other" disabled="disabled">{{$permohonan->data_pemohon->alamat_other}}</textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="kode_pos_other" placeholder="-" disabled="disabled" maxlength="8" value="{{$permohonan->data_pemohon->kode_pos_other}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="nomor_hp_other" placeholder="-" disabled="disabled" maxlength="15" value="{{$permohonan->data_pemohon->no_hp_other}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="email_other" placeholder="-" disabled="disabled" maxlength="30" value="{{$permohonan->data_pemohon->email_other}}">
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      @if($permohonan->data_pemohon_lainnya==null || $permohonan->data_pemohon_lainnya->count()==0)
      <input type="checkbox" name="" id="another_pemohon" onchange="other_person_toggle()" disabled="disabled"> Pemohon Lainnya<br><br>
      @else
      <input type="checkbox" name="" id="another_pemohon" onchange="other_person_toggle()" disabled="disabled" checked="checked"> Pemohon Lainnya<br><br>
      @endif
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <table id="pemohon_tambahan" class="display datatables" style="font-size:10pt">
            <thead style="text-align:center;">
                <tr>
                    <th>No KTP</th>
                    <th>Nama Pemohon Tambahan</th>
                </tr>
            </thead>
            <tbody>
              @foreach($permohonan->data_pemohon_lainnya as $val)
              <tr>
                <td>{{$val->no_ktp}}</td>
                <td>{{$val->nama_pemohon}}</td>
              </tr>
              @endforeach
            </tbody>
        </table>
    </div>
  </div>
  <br>
  <hr>
  <div class="row">
      <div class="col-md-12">
          <h6 class="bold">Data Merek</h6>
      </div>
  </div>
  <br>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tipe Permohonan<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="tipe_permohonan" required="required" disabled="disabled">
        <option value="" disabled="" selected="">{{$permohonan->data_merek->tipe_permohonan->permohonan}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tipe Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="tipe_merek" required="required" disabled="disabled">
        <option value="" disabled="" selected="">{{$permohonan->data_merek->tipe_merek->merek}}</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Label Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      @if($permohonan->data_merek->label_merek!=null)
      <a href="{{route('root')}}/{{$permohonan->data_merek->label_merek}}" target="_BLANK">
        <button class="btn btn-md btn-primary" type="button"><i class="fa fa-external-link"></i> Lihat Label Merek</button>
      </a>
      @else
      <button class="btn btn-md btn-primary" disabled="disabled" type="button"><i class="fa fa-external-link"></i> Lihat Label Merek</button>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama_merek" placeholder="-" required="required" disabled="disabled" value="{{$permohonan->data_merek->merek}}">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Unsur warna dalam Label Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <textarea class="form-control" placeholder="-" rows="3" id="unsur_warna" required="required" disabled="disabled">{{$permohonan->data_merek->unsur_warna_label_merek}}</textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Pilihan Kelas<span class="red">*</span></label>
    <div class="col-sm-2">
      <input type="text" name="" class="form-control" id="jumlah_kelas" disabled="disabled" value="{{$permohonan->jumlah_kelas}}">
    </div>
    <div class="col-sm-1">
      <p style="position:relative;top:5px;left:-15px">Kelas</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <table id="tabel_kelas_dipilih" class="display datatables" style="font-size:10pt">
            <thead>
                <tr>
                    <th>Kelas</th>
                    <th>Deskripsi (INA)</th>
                    <th>Deskripsi (ENG)</th>
                </tr>
            </thead>
            <tbody>
              @foreach($permohonan->data_kelas as $val)
              <tr>
                <td>{{$val->kelas_master->kelas}}</td>
                <td>{{$val->kelas_master->deskripsi_id}}</td>
                <td>{{$val->kelas_master->deskripsi_en}}</td>
              </tr>
              @endforeach
            </tbody>
        </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      @if($permohonan->unknown_class==null || $permohonan->unknown_class=="")
      <input type="checkbox" name="" id="unknown_class" disabled="disabled"> Uraian jenis barang atau jasa:<br><br>
      @else
      <input type="checkbox" name="" id="unknown_class" disabled="disabled" checked="checked"> Uraian jenis barang atau jasa:<br><br>
      @endif
    </div>
  </div>
  <div class="row" style="margin-top:-20px">
    <div class="col-md-12">
      <textarea class="form-control" id="unknown_class_description" placeholder="-" rows="4" disabled="disabled">{{$permohonan->unknown_class}}</textarea>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 right">
      <br>
      <p>Jumlah Kelas: <span class="bold" id="span_jml_kelas">{{$permohonan->jumlah_kelas}}</span> <span class="bold">Kelas</span></p>
    </div>
  </div>
  <br>
  <hr>
  <div class="row">
      <div class="col-md-12">
          <h6 class="bold">Berkas Diunggah</h6>
      </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-12">
      <table class="table-syarat">
        <thead>
          <tr class="bold">
            <td class="col-sm-6">Jenis Berkas</td>
            <td>Upload</td>
          </tr>
        </thead>
        <tbody>
          @foreach($permohonan->data_berkas as $val)
          <tr>
            <td>
              {{$val->syarat_berkas->jenis_berkas}}
            </td>
            <td>
              <a href="{{route('root')}}/{{$val->berkas}}" target="_BLANK">
                <button class="btn btn-sm btn-success" type="button"><i class="fa fa-download"></i> Unduh Berkas</button>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <br>
</form>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@include("layouts.plugins.steps")
<script type="text/javascript">
	/*$("#modal-body").wizard({
		exitText: 'exit',
        onfinish:function(){
            console.log("Hola mundo");
        }
    });*/
</script>
@include("layouts.plugins.select2")
<script type="text/javascript">
	$(document).ready(function() {
    	$('.select2').select2({
        placeholder: function(){
          $(this).data('placeholder');
        }
      });
	});
</script>
@include("layouts.plugins.datatables")
<script type="text/javascript">
    $(document).ready( function () {
        $('#pemohon_tambahan').DataTable({
        	"lengthChange": false,
        	"searching": false,
        	"paging": false,
        	"info": false,
        });
        $('#tabel_kelas_dipilih').DataTable({
          "lengthChange": false,
          "searching": false,
          "info": false,
        });
    });
</script>
@endsection