@extends("layouts.template")

@section("content")
<h3>Selamat Datang <span class="bold">{{Auth::user()->name}}</span></h3>
<br>
<p>Untuk mengakses <span class="bold">panel {{Auth::user()->usertype}}</span>, silahkan:</p>
<ol class="black" style="margin-left:20px">
    <li>1. Sorot <span class="bold">daftar menu</span> pengguna diatas</li>
    <li>2. Pilih <span class="bold">panel {{Auth::user()->usertype}}</span> (atau klik <a href="{{route('dashboard')}}" target="_BLANK">disini!</a>)</p></li>
    <li>3. Selesai</li>
</ol>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@endsection