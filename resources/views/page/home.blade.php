@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12 block_tentang_kami" style="background:url('{{asset('konten/brand-story-new.jpg')}}');text-align:center;padding:100px;background-size:100% 100%;background-repeat:no-repeat;height:500px">
        <div style="font-size:25px; text-align: justify; padding:50px; background: rgba(10, 10, 10, 0.8);color:white;font-family:'Arial';line-height:1.3em" class="tentang_kami_txt">
            Dalam sebuah usaha, merek bisa menjadi aset yang paling berharga apabila sudah dikenal luas oleh konsumen. Sebuah bisnis dapat tumbuh dan menjadi besar karena memanfaatkan kekuatan merek.
        </div>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-md-12 center">
       <p class="bold italic black" style="font-size:21px;margin-bottom:7px;">Segera Lindungi Merek Usaha Anda</p>
       <p class="bold italic black" style="font-size:21px">Agat tidak ditiru dan digunakan oleh orang lain</p>
    </div>
</div>

<div class="hideOnDesktop">
    <br>
    <hr>
    <br>
</div>

<div class="hideOnMobile">
    <br>
</div>

<div class="row black">
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/database.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Free Pengecekan Merek</p>
            <p class="center">Pengecekan database merek penting dilakukan agar tidak menyerupai merek lain yang sudah terdaftar, sehingga terhindar dari penolakan merek oleh Dirjen HKI.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/money-back-guarantee.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Garansi Uang Kembali</p>
            <p class="center">Kami siap menanggung resiko kerugian anda. Bila permohonan merek anda melalui kami ditolak oleh Dirjen HKI, maka kami berikan ganti rugi uang anda 100 persen.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/internet.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Teknologi Termutakhir</p>
            <p class="center">Kepemilikan merek menganut asas First to File (siapa cepat dia dapat), sehingga kami selalu bergerak cepat mendaftarkan merek anda menggunakan platform termutakhir.</p>
        </div>
    </div>
</div>

<br>

<div class="row black">
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/user-icon.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Terbuka untuk Umum</p>
            <p class="center">Siapa saja bebas untuk mendaftarkan merek, baik badan usaha maupun individu yang sudah/belum memiliki usaha. Semua merek dapat didaftarkan meskipun baru berupa ide.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/shop-2.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Terbuka untuk Semua Jenis Usaha</p>
            <p class="center">Semua jenis usaha yang tidak melanggar hukum dapat didaftarkan mereknya. Cocok untuk usaha pertokoan, rumah makan, kafe dan restoran, start up, online shop, UMKM, Usaha Jasa, dsb.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/ktp.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Persyaratan Mudah</p>
            <p class="center">Untuk mendaftarkan merek hanya dibutuhkan persyaratan berupa KTP (untuk kategori umum) atau syarat tambahan berupa surat keterangan UMKM (untuk kategori UMKM).</p>
        </div>
    </div>
</div>

<br>

<div class="row black">
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/shield-ok-icon.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Masa Perlindungan Panjang</p>
            <p class="center">Setiap merek terdaftar akan memperoleh perlindungan selama 10 tahun (terhitung dari tanggal permohonan) dan dapat diperpanjang terus-menerus setiap 10 tahun sekali.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/typing.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Proses Mudah & Cepat</p>
            <p class="center">Cukup mengisi semua data dan uplaod file yang diperlukan. Tidak ada kirim-mengirim dokumen secara fisik. Seluruh proses dapat dilakukan sendiri secara online.</p>
        </div>
    </div>
    <div class="col-md-4 moreMarginAtMobile">
        <div class="center">
            <img src="{{asset('konten/flag.png')}}" width="200px" height="200px" class="image-ads-at-home">
        </div>
        <div style="padding-left:10px;padding-right:10px">
            <p class="center bold uppercase">Skala Nasional</p>
            <p class="center">Jangkauan perlindungan merek mencakup seluruh provinsi di Indonesia. Bila pelanggaran merek terjadi di wilayah manapun di Indonesia, maka dapat diambil tindakan hukum berdasarkan UU yang berlaku.</p>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12 center">
        <a href="{{route('permohonan-user.create')}}">
            <button class="btn-reg-now">
                Daftarkan Merek Sekarang
            </button>
        </a>
    </div>
</div>

<br>

<div class="row hideOnMobile">
    <div class="col-md-4">
        <div>
            <p class="bold black">Tarif Permohonan merek Baru:</p>
            <table class="price-table bold black">
                <tr><td>Umum</td><td>Rp {{number_format($merek_baru_umum->harga)}}{{$merek_baru_umum->satuan_harga}}</td></tr>
                <tr><td>UMKM</td><td>Rp {{number_format($merek_baru_umkm->harga)}}{{$merek_baru_umkm->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div>
            <p class="bold black">Tarif Perpanjangan merek:</p>
            <table class="price-table bold black">
                <tr><td>Umum</td><td>Rp {{number_format($merek_panjang_umum->harga)}}{{$merek_panjang_umum->satuan_harga}}</td></tr>
                <tr><td>UMKM</td><td>Rp {{number_format($merek_panjang_umkm->harga)}}{{$merek_panjang_umkm->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div>
            <p class="bold black">Tarif Permohonan Merek<br> Internasional (WIPO):</p>
            <table class="price-table bold black" style="margin-left:-10px">
                <tr><td>Rp {{number_format($merek_intern->harga)}}{{$merek_intern->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
</div>

<div class="row hideOnDesktop">
    <div class="col-md-12">
        <div>
            <p class="bold black">Tarif Permohonan merek Baru:</p>
            <table class="price-table bold black">
                <tr><td>Umum</td><td>Rp {{number_format($merek_baru_umum->harga)}}{{$merek_baru_umum->satuan_harga}}</td></tr>
                <tr><td>UMKM</td><td>Rp {{number_format($merek_baru_umkm->harga)}}{{$merek_baru_umkm->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div>
            <p class="bold black">Tarif Perpanjangan merek:</p>
            <table class="price-table bold black">
                <tr><td>Umum</td><td>Rp {{number_format($merek_panjang_umum->harga)}}{{$merek_panjang_umum->satuan_harga}}</td></tr>
                <tr><td>UMKM</td><td>Rp {{number_format($merek_panjang_umkm->harga)}}{{$merek_panjang_umkm->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div>
            <p class="bold black">Tarif Permohonan Merek <br>Internasional (WIPO):</p>
            <table class="price-table bold black"">
                <tr><td>Rp {{number_format($merek_intern->harga)}}{{$merek_intern->satuan_harga}}</td></tr>
            </table>
        </div>
    </div>
</div>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@endsection