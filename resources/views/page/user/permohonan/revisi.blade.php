@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold center underline">Permohonan Merek Revisi</h4>
    </div>
</div>

<br>

@if(Auth::user()->credit > 0)
<form action="#" method="POST" enctype="multipart/form-data" id="form_merek_baru" onsubmit="return false">
  {{csrf_field()}}
  <div class="row">
      <div class="col-md-10">
          <h6 class="bold">Data Pemohon</h6>
      </div>
      <div class="col-md-2 right">
        <span class="red">*) Wajib</span>
      </div>
  </div>
  <br>
   <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor KTP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_ktp" placeholder="Nomor KTP / Kosongkan bila pemohon berupa badan hukum" maxlength="16">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Pemohon<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama_pemohon" placeholder="Nama Pemohon / Nama Badan Hukum" maxlength="30" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kewarganegaraan<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="kewarganegaraan" required="required">
        <option value="" disabled="" selected="">--Pilih Kewarganegaraan--</option>
        @foreach($negara as $val)
        <option value="{{$val->id}}">{{$val->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis Pemohon<span class="red">*</span></label>
    <div class="col-sm-10" id="jenis_pemohon">
      @foreach($jenis_pemohon as $val)      
      <input type="radio" name="jenis_pemohon" value="{{$val->id_jenis_pemohon}}" required="required" onchange="toogle_jenis_pemohon()"> {{$val->jenis_pemohon}} 
      &nbsp &nbsp
      @endforeach
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="negara" onchange="getProvinsi()" required="required">
        <option value="" disabled="" selected="">--Pilih Negara--</option>
        @foreach($negara as $val)
        <option value="{{$val->id}}">{{$val->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="provinsi" onchange="getKota()" required="required">
        <option value="" disabled="" selected="" disabled="disabled">--Pilih Provinsi--</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="kota" disabled="disabled" required="required">
        <option value="" disabled="" selected="">--Pilih Kabupaten/Kota--</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat<span class="red">*</span></label>
    <div class="col-sm-10">
      <textarea class="form-control" placeholder="Alamat pemohon sesuai KTP / Alamat domisili badan hukum" rows="3" id="alamat" required="required"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="kode_pos" placeholder="Kode Pos" maxlength="8">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP/Telp<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_hp" placeholder="Nomor HP/Telp" maxlength="15" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="Alamat Email" maxlength="30" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">WhatsApp<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="whatsapp" placeholder="Nomor WhatsApp" maxlength="15" required="required">
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <input type="checkbox" name="" id="another_address" onchange="other_addess_toggle()"> Jika alamat surat menyurat tidak sama dengan Identitas Pemohon<br><br>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="negara_other" onchange="getProvinsiScope('negara_other','provinsi_other')" disabled="disabled">
        <option value="" disabled="" selected="">--Pilih Negara--</option>
        @foreach($negara as $val)
        <option value="{{$val->id}}">{{$val->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="provinsi_other" onchange="getKotaScope('provinsi_other','kota_other')" disabled="disabled">
        <option value="" disabled="" selected="">--Pilih Provinsi--</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota</label>
    <div class="col-sm-10">
      <select class="select2 other_address" id="kota_other" disabled="disabled">
        <option value="" disabled="" selected="">--Pilih Kabupaten/Kota--</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat</label>
    <div class="col-sm-10">
      <textarea class="form-control other_address" placeholder="Alamat pemohon utama" rows="3" id="alamat_other" disabled="disabled"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="kode_pos_other" placeholder="Kode Pos" disabled="disabled" maxlength="8">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="nomor_hp_other" placeholder="Nomor HP" disabled="disabled" maxlength="15">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_address" id="email_other" placeholder="Alamat Email" disabled="disabled" maxlength="30">
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <input type="checkbox" name="" id="another_pemohon" onchange="other_person_toggle()"> Jika identitas Pemohon lebih dari satu<br><br>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">No KTP Pemohon</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_pemohon" id="ktp_permohon_tambahan" placeholder="Nama Pemohon Tambahan" disabled="disabled" maxlength="16">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Pemohon</label>
    <div class="col-sm-10">
      <input type="text" class="form-control other_pemohon" id="nama_permohon_tambahan" placeholder="Nama Pemohon Tambahan" disabled="disabled" maxlength="30">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-12 right">
      <button class="btn btn-sm btn-primary other_pemohon" onclick="tambah_pemohon_tambahan()" type="button">
        <i class="fa fa-plus"></i> Tambah Pemohon Tambahan
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <table id="pemohon_tambahan" class="display datatables" style="font-size:10pt">
            <thead style="text-align:center;">
                <tr>
                    <th>No KTP</th>
                    <th>Nama Pemohon Tambahan</th>
                    <th>#</th>
                </tr>
            </thead>
        </table>
    </div>
  </div>
  <br>
  <hr>
  <div class="row">
      <div class="col-md-12">
          <h6 class="bold">Data Merek</h6>
      </div>
  </div>
  <br>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tipe Permohonan<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="tipe_permohonan" required="required">
        <option value="" disabled="" selected="">--Pilih Tipe Permohonan--</option>
        @foreach($tipe_permohonan as $val)
        <option value="{{$val->id_tipe_permohonan}}">{{$val->permohonan}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tipe Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="tipe_merek" required="required">
        <option value="" disabled="" selected="">--Pilih Tipe Merek--</option>
        @foreach($tipe_merek as $val)
        <option value="{{$val->id_tipe_merek}}">{{$val->merek}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Logo Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="file" class="form-control" id="label_merek" accept=".JPG, .JPEG" required="required">
    </div>
  </div>
  <div class="form-group row" style="margin-top:-15px">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <span class="red">(Format File: Hanya JPG, Max 2MB, Dimensi Gambar Max. 1024 x 1024 Pixel)</span>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama_merek" placeholder="Nama Merek" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Unsur warna dalam Logo Merek<span class="red">*</span></label>
    <div class="col-sm-10">
      <textarea class="form-control" placeholder="Contoh: Hitam; Putih dan Bitu; Kuning, Merah, Hijau dan Biru; dan seterusnya" rows="3" id="unsur_warna" required="required"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Pilihan Kelas<span class="red">*</span></label>
    <div class="col-sm-2">
      <input type="text" name="" class="form-control" id="jumlah_kelas" disabled="disabled" value="0">
    </div>
    <div class="col-sm-1">
      <p style="position:relative;top:5px;left:-15px">Kelas</p>
    </div>
    <div class="col-sm-5 left">
      <button class="btn btn-primary btn-md" onclick="show_modal_data_merek()" type="button">Pilih Kelas</button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <table id="tabel_kelas_dipilih" class="display datatables" style="font-size:10pt">
            <thead>
                <tr>
                    <th>Kelas</th>
                    <th>Deskripsi</th>
                    <th>#</th>
                </tr>
            </thead>
        </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 right">
      <p>Jumlah Kelas: <span class="bold" id="span_jml_kelas">0</span> <span class="bold">Kelas</span></p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <input type="checkbox" name="" id="unknown_class" onchange="unknown_class_toggle()"> Kosongkan pilihan kelas bila tidak tahu, uraikan jenis barang atau jasa:<br><br>
    </div>
  </div>
  <div class="row" style="margin-top:-20px">
    <div class="col-md-12">
      <textarea class="form-control" id="unknown_class_description" placeholder="contoh: toko pakaian, bengkel mobil, apotek, minuman berkarbonasi, makanan ringan, coffe shop, minimarket, toko bangunan, dll" rows="4" disabled="disabled"></textarea>
    </div>
  </div>
  <br>
  <div class="row">
      <div class="col-md-12">
          <h6 class="bold">Upload Berkas</h6>
      </div>
  </div>
  <hr>
  @foreach($jenis_pemohon as $val)
  <div class="form-group row">
    <div class="col-sm-12">
      <input type="radio" name="upload_berkas" value="{{$val->id_jenis_pemohon}}" onchange="toogle_upload_syarat()"> {{$val->jenis_pemohon}}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table-syarat">
        <thead>
          <tr class="bold">
            <td class="col-sm-6">Jenis Berkas</td>
            <td>Upload</td>
          </tr>
        </thead>
        <tbody>
          @foreach($val->syarat_berkas as $syarat)
          <tr>
            <td class="left">{{$syarat->jenis_berkas}}</td>
            <td>
              <input type="file" name="" accept="{{$syarat->file_format}}" disabled="disabled" class="block-syarat syarat-{{$val->id_jenis_pemohon}}" id="syarat_berkas_{{$syarat->id_syarat_berkas}}">
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <br>
  @endforeach
  <div class="row">
      <div class="col-md-12">
          <p class="bold red">* File berkas diambil dengan cara scan/foto</p>
          <p class="bold red">* Tanda tangan dibuat di atas kertas putih dan harus sesuai KTP</p>
          <p class="bold red">* Format file berkas: JPG/JPEG/PDF</p>
          <p class="bold red">* Maksimal file 15 MB</p>
      </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-12">
      <input type="checkbox" name="" id="term_condition"> Dengan ini, saya menyatakan bahwa merek tersebut merupakan milik saya dan tidak meniru merek pihak lain.<br><br>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-sm-12 right">
        <button class="btn btn-md btn-primary" onclick="submit_merek_baru()" type="button">Proses</button>
    </div>
  </div>
</form>
@else
<div class="row">
    <div class="col-md-12 center">
        <h6 class="italic">Oops! Credit Anda tidak cukup untuk melakukan pengajuan permohonan revisi</h6>
    </div>
</div>
@endif
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@include("layouts.plugins.steps")
<script type="text/javascript">
  var $is_revisi          = false;
  var $submit_server      = "{{Route('permohonan-user-revisi.index')}}";
  var $success_submit_url = "{{route('user_page')}}/permohonan_saya";
</script>
<script type="text/javascript">
  /*$("#modal-body").wizard({
    exitText: 'exit',
        onfinish:function(){
            console.log("Hola mundo");
        }
    });*/
</script>
@include("layouts.plugins.select2")
<script type="text/javascript">
  $(document).ready(function() {
      $('.select2').select2({
        placeholder: function(){
          $(this).data('placeholder');
        }
      });
  });
</script>
@include("layouts.plugins.datatables")
<script type="text/javascript">
    $(document).ready( function () {
        $('#pemohon_tambahan').DataTable({
          "lengthChange": false,
          "searching": false,
          "paging": false,
          "info": false,
        });
        $('#tabel_kelas_dipilih').DataTable({
          "lengthChange": false,
          "searching": false,
          "paging": false,
          "info": false,
        });
    });
</script>
@include("additional.js.alamat")
@include("additional.js.pemohon")
@include("additional.js.syarat_upload")
@include("additional.blade.data_merek")
@include("additional.js.submit_merek_baru")
@include("additional.js.unknown_class")
@endsection