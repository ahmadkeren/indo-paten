@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Permohonan Merek Baru</h4>
    </div>
</div>

<br>

<div class="row step-row">
    <div class="col-md-12">
    	@php $step = 1; @endphp
    	@include("layouts.parts.steps_permohonan")
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-10">
        <h6 class="bold">Data Pemohon</h6>
    </div>
    <div class="col-md-2 right">
    	<span class="red">*) Wajib</span>
    </div>
</div>

<br>

<form>
   <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor KTP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_ktp" placeholder="Nomor KTP">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Pemohon<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama_pemohon" placeholder="Nama Pemohon">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis Pemohon<span class="red">*</span></label>
    <div class="col-sm-10" id="jenis_pemohon">
      <input type="radio" name="jenis_pemohon" value="perorangan" required="required"> Perorangan &nbsp &nbsp
      <input type="radio" name="jenis_pemohon" value="umkm" required="required"> UMKM &nbsp &nbsp
      <input type="radio" name="jenis_pemohon" value="badan_hukum" required="required"> Badan Hukum
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="negara">
      	<option>Indonesia</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="provinsi">
      	<option>Jambi</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota<span class="red">*</span></label>
    <div class="col-sm-10">
      <select class="select2" id="kota">
      	<option>Kota Jambi</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat<span class="red">*</span></label>
    <div class="col-sm-10">
    	<textarea class="form-control" placeholder="Alamat pemohon utama" rows="3" id="alamat"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="kode_pos" placeholder="Kode Pos">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_hp" placeholder="Nomor HP">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="Alamat Email">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">WhatsApp<span class="red">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="whatsapp" placeholder="Nomor WhatsApp">
    </div>
  </div>
  <hr>
  <div class="row">
  	<div class="col-sm-12">
  		<input type="checkbox" name="" id="another_address"> Jika alamat surat menyurat tidak sama dengan Identitas Pemohon<br><br>
  	</div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Negara</label>
    <div class="col-sm-10">
      <select class="select2" id="negara_2" disabled="disabled">
      	<option>Indonesia</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi</label>
    <div class="col-sm-10">
      <select class="select2" id="provinsi_2" disabled="disabled">
      	<option>Jambi</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kabupaten/Kota</label>
    <div class="col-sm-10">
      <select class="select2" id="kota_2" disabled="disabled">
      	<option>Kota Jambi</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat</label>
    <div class="col-sm-10">
    	<textarea class="form-control" placeholder="Alamat pemohon utama" rows="3" id="alamat_2" disabled="disabled"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kode Pos</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="kode_pos_2" placeholder="Kode Pos" disabled="disabled">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor HP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nomor_hp_2" placeholder="Nomor HP" disabled="disabled">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email_2" placeholder="Alamat Email" disabled="disabled">
    </div>
  </div>
  <hr>
  <div class="row">
  	<div class="col-sm-12">
  		<p>Identitas Pemohon Lebih dari Satu</p><br>
  	</div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Pemohon</label>
    <div class="col-sm-8">
    	<input type="text" class="form-control" id="nama_permohon_tambahan" placeholder="Nama Pemohon Tambahan">
    </div>
    <div class="col-sm-2">
    	<button class="btn btn-sm btn-primary">
    		<i class="fa fa-plus"></i>
    	</button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <table id="table_id" class="display datatables" style="font-size:10pt">
            <thead style="text-align:center;">
                <tr>
                    <th>No.</th>
                    <th>Nama Pemohon Tambahan</th>
                    <th>#</th>
                </tr>
            </thead>
        </table>
    </div>
  </div>
</form>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@include("layouts.plugins.steps")
<script type="text/javascript">
	$("#modal-body").wizard({
		exitText: 'exit',
        onfinish:function(){
            console.log("Hola mundo");
        }
    });
</script>
@include("layouts.plugins.select2")
<script type="text/javascript">
	$(document).ready(function() {
    	$('.select2').select2();
	});
</script>
@include("layouts.plugins.datatables")
<script type="text/javascript">
    $(document).ready( function () {
        $('#table_id').DataTable({
        	"lengthChange": false,
        	"searching": false,
        	"paging": false,
        	"info": false,
        });
    });
</script>
@endsection