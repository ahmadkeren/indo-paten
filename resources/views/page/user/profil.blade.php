@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Profil Saya</h4>
    </div>
</div>

<br>

<form>
   <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Lengkap</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" placeholder="" value="{{Auth::user()->name}}" disabled="disabled">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Alamat Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" placeholder="Email sebelumnya: {{Auth::user()->email}}" value="{{Auth::user()->email}}" id="email">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor Ponsel</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" placeholder="" value="{{Auth::user()->ponsel}}" id="ponsel" maxlength="20">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Password Baru</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" placeholder="Masukkan kata sandi baru" id="new_password">
    </div>
  </div>
  <div class="form-group row" style="margin-top:-15px">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <span class="red">(Hanya isi jika ingin mengganti kata sandi)</span>
    </div>
  </div>
  <hr>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Kata Sandi Saat Sekarang</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" placeholder="Masukkan kata sandi saat ini" id="current_password">
    </div>
  </div>
  <div class="form-group row" style="margin-top:-15px">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <span class="red">(Dipelukan untuk mengupdate perubahan)</span>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 right">
      <button class="btn btn-md btn-info" type="button" onclick="update_profil()">Update Profil</button>
    </div>
  </div>
</form>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
<script type="text/javascript">
  function update_profil() {
    $email = $("#email").val();
    $ponsel = $("#ponsel").val();
    $new_password = $("#new_password").val();
    $current_password = $("#current_password").val();
    if(!handle_require($email,"Alamat email")) return;
    if(!handle_require($ponsel,"Nomor ponsel")) return;
    if(!handle_require($current_password,"Kata sandi saat ini")) return;
    showLoading();
    $.ajax({
      url    : "{{route('user_profile.index')}}",
      method : "POST",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      data   : {
        "email" : $email,
        "ponsel" : $ponsel,
        "new_password" : $new_password,
        "current_password" : $current_password,
        "_token": "{{ csrf_token() }}",
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          showToast("success",res.pesan);
          $("#new_password").val("");
          $("#current_password").val("");
        } else
          showToast("error",res.pesan);
      },
      error: function() {
        hideLoading();
        showToast("error","Tidak dapat terhubung ke server!");
      }
    })
  }
</script>
@endsection