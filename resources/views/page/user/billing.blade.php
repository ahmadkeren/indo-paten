@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Cart</h4>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12">
        <table id="tabel_billing" class="responsive display nowrap datatables" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th data-priority="1">Invoice#</th>
                    <th>Tanggal</th>
                    <th>Merek</th>
                    <th>Jumlah Kelas</th>
                    <th data-priority="3">Total</th><!-- 
                    <th>Upload Bukti</th> -->
                    <th>Status</th>
                    <th data-priority="2">Pembayaran</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form action="#" method="POST" id="upload_bukti" style="display:none">
    <input type="file" name="photo" onchange="upload_bukti()" accept="image/*,.PDF" id="choosen_file">
</form>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@include("layouts.plugins.datatables")
<script type="text/javascript">
    $(document).ready( function () {
        $tabel_billing = $("#tabel_billing").DataTable({
            "ajax": {
                url: "{{Route('service')}}/getBillingSaya/{{Auth::user()->id}}"
            },
            "processing": true,
            "serverSide": true,
            "columns": [
                {"data": "id_permohonan", render: function (data, type, row, meta) {
                    //return meta.row + meta.settings._iDisplayStart + 1;
                    return "<a href='{{route('invoice')}}/"+data+"' target='_BLANK'>INV"+data+"</a>";
                }, searchable: false},
                { "data": "tanggal_pengajuan", searchable: false},
                { "data": "data_merek.merek" },
                { "data": "jumlah_kelas", render: function (data, type, row, meta) {
                    return data+" kelas";
                }},
                { "data": "harga", searchable: false, orderable: false},/*
                { "data": "id_permohonan", render: function (data, type, row, meta) {
                    $payment_id = row['payment_id'];
                    if(row['pembayaran_dikonfirmasi'])
                        return "<button class='btn btn-success btn-sm' disabled='disabled'>Unduh Bukti</button>";
                    if($payment_id==null || $payment_id=="" || $payment_id==NaN)
                        return "<button class='btn btn-primary btn-sm' disabled='disabled'>Upload Bukti</button>";
                    $bukti_pembayaran = row['bukti_pembayaran'];
                    if($bukti_pembayaran!=null && $bukti_pembayaran!="")
                        return "<a href='{{route('root')}}/"+$bukti_pembayaran+"' class='btn btn-success btn-sm' target='_BLANK'>Unduh Bukti</a>";
                    else
                        return "<button class='btn btn-info btn-sm' target='_BLANK' onclick='choose_to_upload("+data+")'>Upload Bukti</button>";
                }, searchable: false, orderable: false},*/
                { "data": "status_billing", searchable: false, orderable: false},
                { "data": "need_generate_payment", render: function (data, type, row, meta) {
                    $pembayaran_dikonfirmasi = row['pembayaran_dikonfirmasi'];
                    $payment_id = row['payment_id'];
                    if($pembayaran_dikonfirmasi==true || $pembayaran_dikonfirmasi=="1")
                        return "<button class='btn btn-sm btn-primary' disabled='disabled'>Sudah Dibayar</button>";
                    if(data==true || data==1 || data=="1") {
                        return "<a class='btn btn-success btn-sm' href='{{route('bayar_merek_baru')}}/"+row['id_permohonan']+"'>Lakukan Pembayaran</a>";
                    } else {
                        if($payment_id==null || $payment_id==NaN || $payment_id=="") {
                            return "<a class='btn btn-success btn-sm' href='{{route('bayar_merek_baru')}}/"+row['id_permohonan']+"'>Lakukan Pembayaran</a>";
                        } else
                            return "<a class='btn btn-info btn-sm' href='"+row['invoice_url']+"' target='_BLANK'>Lihat Pembayaran</a>";
                    }
                }, searchable: false, orderable: false},
            ],
            "order": [[1, 'desc']]
        });
    });

    $tmp_target_permohonan = null;
    function choose_to_upload($id_permohonan) {
        $tmp_target_permohonan = $id_permohonan;
        $("#upload_bukti input").click();
    }

    function upload_bukti() {
        showLoading();
        $file_data = $("#choosen_file").prop('files')[0];
        $form_data = new FormData();
        $form_data.append('bukti_pembayaran',$file_data);
        $form_data.append('id_permohonan',$tmp_target_permohonan);
        $.ajax({
            url    : "{{Route('permohonan-user.index')}}/update",
            method : "POST",
            cache  : false,
            contentType: false,
            processData: false,
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : $form_data,
            success: function(res) {
                hideLoading();
                if(res.success) {
                    showToast("success",res.pesan);
                    $("#tabel_billing").DataTable().ajax.reload();
                } else
                    showToast("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                showToast("error","Tidak dapat terhubung ke server!");
            }
        })
    }
</script>
@endsection