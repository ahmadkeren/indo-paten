@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Permohonan Saya</h4>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-md btn-info" href="{{Route('permohonan-user.create')}}">
            <i class="fa fa-plus"></i> Tambah
        </a>
        <br><br>
    </div>
    <div class="col-md-12">
        <table id="tabel_permohonan_saya" class="responsive display nowrap datatables" cellspacing="0" width="100%" style="font-size:10pt">
            <thead>
                <tr>
                    <th data-priority="1">No.</th>
                    <th data-priority="2">Invoice#</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Tipe Merek</th>
                    <th data-priority="4">Merek</th>
                    <th>Kelas</th>
                    <th>No. Permohonan</th><!-- 
                    <th>Tipe Permohonan</th>
                    <th>Jenis Permohonan</th> -->
                    <th data-priority="3">Status</th>
                    <th>Unduh</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@include("layouts.plugins.datatables")
<script type="text/javascript">
    $(document).ready( function () {
        $tabel_permohonan_saya = $("#tabel_permohonan_saya").DataTable({
            "ajax": {
                url: "{{Route('service')}}/getPermohonanSaya/{{Auth::user()->id}}"
            },
            "processing": true,
            "serverSide": true,
            "columns": [
                {"data": "id_permohonan", render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }, searchable: false},
                {"data": "id_permohonan", render: function (data, type, row, meta) {
                    return "<a href='{{route('invoice')}}/"+data+"' target='_BLANK'>INV"+data+"</a>";
                }, searchable: false},
                { "data": "tanggal_pengajuan", searchable: false},
                { "data": "data_merek.tipe_merek.merek" },
                { "data": "data_merek.merek" },
                { "data": "jumlah_kelas", render: function (data, type, row, meta) {
                    return data+" kelas";
                }, searchable: false},
                { "data": "nomor_permohonan", render: function (data, type, row, meta) {
                    if(data==null || data=="")
                        return "-";
                    else
                        return data;
                }},/*
                { "data": "data_merek.tipe_permohonan.permohonan" },
                { "data": "data_pemohon.jenis_pemohon.jenis_pemohon" },*/
                { "data": "status_permohonan.status"},
                 { "data": "id_permohonan", render: function (data, type, row, meta) {
                    if(row['tanda_terima']!=null && row['tanda_terima']!="")
                        $btn_tanda_terima = "<a href='{{route('root')}}/"+row['tanda_terima']+"' target='_BLANK' class='white btn btn-sm btn-info'>Tanda Terima</a>";
                    else
                        $btn_tanda_terima = "<button class='btn btn-sm btn-info' disabled='disabled' title='Belum tersedia'>Tanda Terima</button>";

                    /*if(row['surat_pernyataan']!=null && row['surat_pernyataan']!="")
                        $btn_surat_pernyataan = "<a href='{{route('root')}}/"+row['surat_pernyataan']+"' target='_BLANK' class='white btn btn-sm btn-primary'>Surat Pernyataan</a>";
                    else
                        $btn_surat_pernyataan = "<button class='btn btn-sm btn-primary' disabled='disabled' title='Belum tersedia'>Surat Pernyataan</button>";*/
                    $btn_surat_pernyataan = "";
                    return $btn_tanda_terima+"<div></div>"+$btn_surat_pernyataan;
                }, searchable: false, orderable: false},
            ],
            "order": [[1, 'desc']]
        });
    });
</script>
@endsection