<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="En">
<head>
    <script type="text/javascript" src="{{asset('app-assets-custom\plugins/404_pc/content/--op-start-head.js')}}"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="description" content="{{ env('APP_DESCRIPTION', '-') }}">
    <meta name="keywords" content="{{ env('APP_KEYWORDS') }}">
    <meta name="author" content="{{ env('APP_AUTHOR', 'Ahmad Saparudin') }}">
    <title>{{ config('app.name', 'Ahmad Saparudin') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{asset('app-assets-custom\plugins/404_pc/content/normalize.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('app-assets-custom\plugins/404_pc/content/style.css')}}" type="text/css">
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter44122239 = new Ya.Metrika({
                        id:44122239,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="{{asset('app-assets-custom\plugins/404_pc/content/1.gif')}}" style="position:absolute; left:-9999px;" alt="" /></div>
        </noscript>
    <script type="text/javascript" src="{{asset('app-assets-custom\plugins/404_pc/content/--op-end-head.js')}}" defer></script>
</head>

<body>
    <section id="animation">
        <div class="anim-icon" id="404"></div>
    </section>
    <section id="info">
        <h1>Maaf, halaman tidak ditemukan</h1>
        <p>Pastikan link yang Anda masukkan sesuai</p>
    </section>
    <section id="data">
        <div class="data">
            <div class="go-home">
                <a href="{{Route('home')}}">« Go Home</a>
            </div>
        </div>
    </section>
    <script src="{{asset('app-assets-custom\plugins/404_pc/content/animation404.js')}}" type="text/javascript"></script>
</body>
</html>
