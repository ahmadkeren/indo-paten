@if(isset($step))
      @php $step = 1; @endphp
@endif
@php
      $steps = [
         ["step" => "Data Pemohon"],
         ["step" => "Data Merek"],
         ["step" => "Data Kelas"],
         ["step" => "Upload Berkas"],
         ["step" => "Review"],
         ["step" => "Pembayaran"],
      ];
@endphp
<div class="modal-body wizard-content">
      <div class="wizard-steps-panel steps-quantity-6">
      	@for($i=1;$i<=count($steps);$i++)
      	@if($i<$step)
      	<div class="step-number step-{{$i}} done">
      	@elseif($i==$step)
      	<div class="step-number step-{{$i}} doing">
      	@else
      	<div class="step-number step-{{$i}}">
      	@endif
      		<div class="number number-step">{{$i}}</div>
                  <div class="text"> {{$steps[$i-1]['step']}} </div>
      	</div>
      	@endfor
      </div>
</div>