<link href="{{asset('assets\template\css\bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets\template\other\css.css')}}" rel="stylesheet">
<link href="{{asset('assets\template\css\style.css')}}" rel="stylesheet">
<link href="{{asset('assets\template\css\responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets\template\css\color-switcher-design.css')}}" rel="stylesheet">   
<link id="theme-color-file" href="{{asset('assets\template\css\color-themes\default-theme.css')}}" rel="stylesheet">
<link href="{{asset('assets\custom\style.css')}}" rel="stylesheet">
<link rel="shortcut icon" href="{{asset('assets\template\images\favicon.png')}}" type="image/x-icon">
<link rel="icon" href="{{asset('assets\template\images\favicon.png')}}" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">