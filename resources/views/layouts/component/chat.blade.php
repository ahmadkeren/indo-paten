<span class="icon_right_chat">
    <img src="{{asset('img')}}/contact_us.png" onclick="showModalSendEmail()" class="pointer" height="90px" width="90px">
</span>
<!-- Modal Kirim ke Email -->
<div class="modal fade text-left col-md" id="modal_send_email" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary white">
        <h5 class="modal-title white" id="myModal">
            <i class="fa fa-comments-o"></i> Hubungi Kami
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Department</label>
            <div class="col-sm-8">
              <select class="form-control" id="send_mail_receiver_email">
                  <option value="" selected="" disabled="">Pilih Department</option>
                  <option value="{{env('EMAIL_SALES','')}}">Sales</option>
                  <option value="{{env('EMAIL_BILLING','')}}">Billing</option>
                  <option value="{{env('EMAIL_CS','')}}">Customer Service (CS)</option>
              </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Nama Lengkap</label>
            <div class="col-sm-8">
              <input type="text" name="" value="" placeholder="Masukkan nama lengkap" id="send_mail_sender_name" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Nomor Ponsel</label>
            <div class="col-sm-8">
              <input type="text" name="" value="" placeholder="Masukkan nomor ponsel" id="send_mail_sender_phone" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Alamat Email</label>
            <div class="col-sm-8">
              <input type="text" name="" value="" placeholder="Masukkan alamat email" id="send_mail_sender_email" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Pertanyaan Anda</label>
            <div class="col-sm-8">
              <textarea class="form-control" id="send_mail_sender_question" rows="4" placeholder="Masukkan pertanyaan Anda"></textarea>
            </div>
        </div>
      </div>
      <div class="modal-footer left">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
          Tutup dan Batal
        </button>
        <button type="button" class="btn btn-outline-primary" onclick="submit_mail_question()">
          Kirim Pertanyaan
        </button>
        <input type="hidden" name="" id="target_id">
      </div>
    </div>
  </div>
</div>
<!-- Modal Kirim ke Email -->
<script type="text/javascript">
    $(document).ready(function(){
        //$("#modal_send_email").modal("show");
    })
    function showModalSendEmail() {
        $("#modal_send_email").modal("show");
    }
    function reset_mail_question() {
        $("#send_mail_receiver_email").val("");
        $("#send_mail_sender_name").val("");
        $("#send_mail_sender_phone").val("");
        $("#send_mail_sender_email").val("");
        //$("#send_mail_sender_question").val("");
    }
    function submit_mail_question() {
        $receiver_email = $("#send_mail_receiver_email").val();
        $sender_name = $("#send_mail_sender_name").val();
        $sender_phone = $("#send_mail_sender_phone").val();
        $sender_email = $("#send_mail_sender_email").val();
        $sender_question = $("#send_mail_sender_question").val();
        // Validasi
        if(!handle_select($receiver_email,"Department")) return;
        if(!handle_require($sender_name,"Nama lengkap")) return;
        if(!handle_require($sender_phone,"Nomor ponsel")) return;
        if(!handle_require($sender_email,"Alamat email")) return;
        if(!handle_require($sender_question,"Pertanyaan")) return;
        // Submit
        showLoading();
        $.ajax({
            url    : "{{Route('service')}}/sendQuestionToEmail",
            method : "POST",
            data   : {
                "receiver_email" : $receiver_email,
                "sender_name" : $sender_name,
                "sender_phone" : $sender_phone,
                "sender_email" : $sender_email,
                "sender_question" : $sender_question,
            },
            success: function(res) {
              hideLoading();
              if(res.success) {
                showToast("success",res.pesan);
                reset_mail_question();
                $("#modal_send_email").modal("hide");
              } else
                showToast("error",res.pesan);
            },
            error  : function() {
              hideLoading();
              showToast("error","Pesan tidak dapat terkirim! Coba periksa ulang inputan atau koneksi internet!");
            }
          })
    }
</script>