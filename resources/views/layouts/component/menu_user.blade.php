@if(Auth::user())
<div style="font-size:25px;margin-top:0px" class="hideOnMobile">
 <ul class="user_navigation">
    @if(Auth::user()->usertype=="user")
    <li class="margin-right-20">
        <a href="{{route('user_page')}}/mail"> 
            @php $unread_mail = Auth::user()->user->unread_customer_message_count(); @endphp
            @if($unread_mail>0)
            <div class="tooltip pointer" title="Anda memiliki {{$unread_mail}} pesan baru yang belum dibaca">
                <!-- Sistem Shadow -->
                <img src="{{asset('assets/icon/mini/wallet.png')}}" style="width:25px;height:25px">
                <div class="bulp-notif bulp-notif-bottom"><span>1</span></div>
            </div>
            @endif
            <a href="{{route('user_page')}}/mail"><i class="fa fa-envelope white"></i></a>
            @if($unread_mail>0)
            <div class="bulp-notif bulp-notif-bottom"><span>{{$unread_mail}}</span></div>
            @endif
        </a>
    </li>
    <li class="margin-right-20">
        <a href="{{route('permohonan-user-revisi.create')}}">
            @if(Auth::user()->credit>0)
            <div class="tooltip" title="Anda memiliki {{Auth::user()->credit}} credit untuk mendaftarkan {{Auth::user()->credit}} kelas merek">
                <!-- Sistem Shadow -->
                <img src="{{asset('assets/icon/mini/wallet.png')}}" style="width:25px;height:25px">
                <div class="bulp-notif bulp-notif-bottom"><span>1</span></div>
            </div>
            @endif
            <img src="{{asset('assets/icon/mini/wallet.png')}}" style="width:25px;height:25px">
            @if(Auth::user()->credit>0)
            <div class="bulp-notif bulp-notif-bottom"><span>{{Auth::user()->credit}}</span></div>
            @endif
        </a>
    </li>
    <li class="margin-right-20">
        <a href="{{route('user_page')}}/billing">
            @php $unpayed_request = Auth::user()->unpayed_request(); @endphp
            @if($unpayed_request>0)
            <div class="tooltip" title="Anda memiliki {{$unpayed_request}} permohonan yang belum dibayarkan">
                <!-- Sistem Shadow -->
                <i class="fa fa-shopping-cart white" style="width:25px; height:25px"></i>
                <div class="bulp-notif bulp-notif-bottom"><span>1</span></div>
            </div>
            @endif
            <i class="fa fa-shopping-cart white" style="width:25px; height:25px"></i>
            @if($unpayed_request>0)
            <div class="bulp-notif bulp-notif-bottom"><span>{{$unpayed_request}}</span></div>
            @endif
        </a>
    </li>
    @endif
    <li class="dropdown-profile">
        <button class="btn-profil">
            <i class="fa fa-user white"></i> 
            <span class="white"> {{ strlen(Auth::user()->name) > 7 ? substr(Auth::user()->name,0,7)."..." : Auth::user()->name }} </span>
            <i class="fa fa-chevron-down white" style="font-size:15px;top:-5px;position:relative;"></i>
        </button>
        @if(Auth::user()->usertype=="user")
        <div class="dropdown-profile-content">
          <!-- <a href="{{route('user_page')}}/billing">Invoice</a> -->
          <a href="{{route('user_page')}}/permohonan_saya">Permohonan Saya</a>
          <a href="{{route('user_page')}}/profil">Profil</a>
          <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
          </a>
        </div>
        @elseif(Auth::user()->usertype=="admin")
        <div class="dropdown-profile-content">
          <a href="{{route('dashboard')}}" target="_BLANK">Panel Admin</a>
          <a href="{{route('user_page')}}/profil">Profil</a>
          <!-- <a href="{{route('admin_page')}}/permohonan_masuk">Permohonan Masuk</a> -->
          <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
          </a>
        </div>
        @else
        <div class="dropdown-profile-content">
          <a href="{{route('dashboard')}}" target="_BLANK">Panel {{ucfirst(Auth::user()->usertype)}}</a>
          <a href="{{route('user_page')}}/profil">Profil</a>
          <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
          </a>
        </div>
        @endif
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
    </li>
 </ul>
</div>
@else
<div class="hideOnMobile">
    <ul class="navigation clearfix">
        <li><a href="{{route('register')}}" style="color:orange">Sign Up</a></li>
        <li><a href="{{route('login')}}" style="color:orange">Sign In</a></li>
    </ul>
</div>
@endif