@if(Auth::user())
	@if(Auth::user()->usertype=="user")
	<li class="hideOnDesktop">
	    <a href="{{route('user_page')}}/mail">Mail</a>
	</li class="hideOnDesktop">
	<li class="hideOnDesktop">
	    <a href="{{route('permohonan-user-revisi.create')}}">Permohonan Revisi</a>
	</li class="hideOnDesktop">
	<li class="hideOnDesktop">
	    <a href="{{route('user_page')}}/billing">Cart</a>
	</li>
	@endif
	<li class="dropdown hideOnDesktop"><a href="#">{{ strlen(Auth::user()->name) > 7 ? substr(Auth::user()->name,0,7)."..." : Auth::user()->name }}</a>
	    <ul>
	    	@if(Auth::user()->usertype=="user")
	    	<li><a href="{{route('user_page')}}/permohonan_saya">Permohonan Saya</a></li>
			<li><a href="{{route('user_page')}}/profil">Profil</a></li>
			<li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
	    	@elseif(Auth::user()->usertype=="admin")
	    	<li><a href="{{route('dashboard')}}" target="_BLANK">Panel Admin</a></li>
			<li><a href="{{route('user_page')}}/profil">Profil</a></li>
			<li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
	    	@else
	    	<li><a href="{{route('dashboard')}}" target="_BLANK">Panel {{ucfirst(Auth::user()->usertype)}}</a></li>
			<li><a href="{{route('user_page')}}/profil">Profil</a></li>
			<li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
	    	@endif
	    </ul>
	</li>
@else
<li class="hideOnDesktop"><a href="{{route('register')}}" style="color:yellow">Sign Up</a></li>
<li class="hideOnDesktop"><a href="{{route('login')}}" style="color:yellow">Sign In</a></li>
@endif