@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12" style="padding-left:45px">
        <p class="bold uppercase underline">Daftar ke Sistem</p>
    </div>
</div>

<br>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error red' : '' }}">
                            <label for="name" class="col-md-8 control-label">Nama Lengkap</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Masukkan nama lengkap">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error red' : '' }}">
                            <label for="email" class="col-md-8 control-label">Alamat E-Mail</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Masukkan alamat email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email_confirmation') ? ' has-error red' : '' }}">
                            <label for="email_confirmation" class="col-md-8 control-label">Konfirmasi E-Mail</label>

                            <div class="col-md-8">
                                <input id="email_confirmation" type="email" class="form-control" name="email_confirmation" value="{{ old('email_confirmation') }}" required placeholder="Masukkan ulang alamat email">

                                @if ($errors->has('email_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ponsel') ? ' has-error red' : '' }}">
                            <label for="ponsel" class="col-md-8 control-label">Nomor Ponsel</label>

                            <div class="col-md-8">
                                <input id="ponsel" type="text" class="form-control" name="ponsel" value="{{ old('ponsel') }}" required placeholder="Masukkan nomor ponsel" maxlength="20">

                                @if ($errors->has('ponsel'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ponsel') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ponsel_confirmation') ? ' has-error red' : '' }}">
                            <label for="ponsel_confirmation" class="col-md-8 control-label">Konfirmasi Nomor Ponsel</label>

                            <div class="col-md-8">
                                <input id="ponsel_confirmation" type="text" class="form-control" name="ponsel_confirmation" value="{{ old('ponsel_confirmation') }}" required placeholder="Masukkan ulang nomor ponsel" maxlength="20">

                                @if ($errors->has('ponsel_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ponsel_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error red' : '' }}">
                            <label for="password" class="col-md-8 control-label">Kata Sandi</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Masukkan kata sandi">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-8 control-label">Konfirmasi Sandi</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Masukkan ulang kata sandi">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                Sudah punya akun? <a href="{{route('login')}}">Login Sekarang</a>!
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
