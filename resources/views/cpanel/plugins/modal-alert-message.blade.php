<!-- Modal -->
<div class="modal fade text-left" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header bg-warning white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-comments"></i> Percakapan</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <h5><i class="la la-lightbulb-o"></i> Tips dan Informasi</h5>
	      <p>Untuk melakukan percakapan dengan Alumni silahkan download dan gunakan aplikasi "{{env('APP_NAME','Circle - UNJA Alumni Tracer')}} Mobile".
	      </p>
	      <div class="alert alert-warning" role="alert">
	        Aplikasi "{{env('APP_NAME','Circle - UNJA Alumni Tracer')}} Mobile" dapat diunduh melalui <a href="{{Route('download')}}" target="_BLANK" class="bold black">link ini</a>.
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
	})
	function showMessage($id_alumni) {
		$("#modal-info").modal("show");
	}
</script>