<script type="text/javascript">
  $(document).ready(function(){
    $result_table = $("#result_table").DataTable({
      "scrollX": true,
      "columns": [
        {searcable: false, orderable: false}, //select
        {searcable: false}, //no
        null, //nama
        null, //jenis
        null, //id
        null, //tanggal
        {searcable: false, orderable: false}, //useetv
        {searcable: false, orderable: false}, //inet
        {searcable: false, orderable: false}, //voice
        null, //status
        {searcable: false, orderable: false}, //assign
        null, //ket
        @if(Auth::user()->isAdmin())
        {searcable: false, orderable: false}, //action
        @endif
      ],
      "order": [[1, 'asc']],
    });
    $('#select_all').on('click', function(){
      var rows = $result_table.rows({ 'search': 'applied' }).nodes();
      $('input.selected_record', rows).prop('checked', this.checked);
    });
    $('#result_table tbody').on('change', 'input.selected_record', function(){
      if(!this.checked){
         var el = $('#select_all').get(0);
         if(el && el.checked && ('indeterminate' in el)){
            el.indeterminate = true;
         }
      }
   });
  })
</script>
<script type="text/javascript">
  function toggle_check() {
    if ($('#select_all').is(':checked')) {
      $("input.selected_record").attr("checked","checked");
    }
    else {
      $("input.selected_record").removeAttr("checked");
    }
  }
  function print_selected_records() {
    $selected_record = [];
    $result_table.$('input.selected_record:checked').each(function() {    
        $selected_record.push($(this).val());
    });
    if($selected_record.length>=1) {
      $print_url = "{{route('print_pelanggan')}}/"+$selected_record,
      window.open($print_url,"_BLANK");
    } else
      miniNotif("warning","Silahkan tandai record yang ingin dicetak terlebih dahulu!");
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('input[name="tanggal"]').daterangepicker();
  })
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#my_table").DataTable({
       "scrollX": true
    });
  })
</script>
<script type="text/javascript">
  function get_witel() {
    $regional = $("#regional").val();
    showLoading();
    $.ajax({
        url    :"{{Route('data_master')}}/get_witel/"+$regional,
        success: function(res) {
          $res = '<option value="all" selected="">Seluruh Witel</option>';
          if(res.length>0) {
            for($i=0;$i<res.length;$i++) {
              $res+='<option value="'+res[$i].witel+'">'+res[$i].witel+'</option>';
            }
          };
          $("#witel").html($res);
          hideLoading();
        },
        error: function() {
          hideLoading();
        },
    })
  }
  function get_datel() {
    $witel = $("#witel").val();
    showLoading();
    $.ajax({
        url    :"{{Route('data_master')}}/get_datel/"+$witel,
        success: function(res) {
          $res = '<option value="all" selected="">Seluruh Datel</option>';
          if(res.length>0) {
            for($i=0;$i<res.length;$i++) {
              $res+='<option value="'+res[$i].datel+'">'+res[$i].datel+'</option>';
            }
          };
          $("#datel").html($res);
          hideLoading();
        },
        error: function() {
          hideLoading();
        },
    })
  }
  function get_sto() {
    $datel = $("#datel").val();
    showLoading();
    $.ajax({
        url    :"{{Route('data_master')}}/get_sto/"+$datel,
        success: function(res) {
          $res = '<option value="all" selected="">Seluruh STO</option>';
          if(res.length>0) {
            for($i=0;$i<res.length;$i++) {
              $res+='<option value="'+res[$i].id_regional+'">'+res[$i].sto+'</option>';
            }
          };
          $("#sto").html($res);
          hideLoading();
        },
        error: function() {
          hideLoading();
        },
    })
  }
</script>
<script type="text/javascript">
  function search_pelanggan() {
    $tanggal = $("#tanggal").val();
    $regional = $("#regional").val();
    $witel = $("#witel").val();
    $datel = $("#datel").val();
    $sto = $("#sto").val();
    $layanan = $("#layanan").val();
    $kelengkapan = $("#kelengkapan").val();
    if(!handle_require($tanggal,"Tanggal")) return;
    if(!handle_select($regional,"Regional")) return;
    if(!handle_select($witel,"Witel")) return;
    if(!handle_select($datel,"Datel")) return;
    if(!handle_select($sto,"STO")) return;
    if(!handle_select($layanan,"Layanan")) return;
    if(!handle_select($kelengkapan,"Kelengkapan")) return;
    // Block
    /*miniNotif("info","Fitur pencarian menunggu integrasi sistem!");
    return;*/
    // Block
    $link_target = "{{Route('home')}}?search=true";
    $link_target+= "&tanggal="+$tanggal;
    $link_target+= "&regional="+$regional;
    $link_target+= "&witel="+$witel;
    $link_target+= "&datel="+$datel;
    $link_target+= "&sto="+$sto;
    $link_target+= "&layanan="+$layanan;
    $link_target+= "&kelengkapan="+$kelengkapan;
    showLoading();
    redirect($link_target);
  }
</script>