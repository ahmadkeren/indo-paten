<script type="text/javascript">
	function handle_select($string,$filed) {
		if($string=="" || $string==null || $string==NaN) {
			miniNotif("warning",$filed+" belum dipilih!");
			return false;
		} else
			return true;
	}
	function handle_require($string,$filed) {
		if($string=="" || $string.trim().length==0) {
			miniNotif("warning",$filed+" belum diinput!");
			return false;
		} else
			return true;
	}
	function handle_char($string,$field,$char_length) {
		if($string=="" || $string.length<$char_length) {
			miniNotif("warning",$field+" setidaknya terdiri dari "+$char_length+" karakter!");
			return false;
		}
			return true;
	}
</script>