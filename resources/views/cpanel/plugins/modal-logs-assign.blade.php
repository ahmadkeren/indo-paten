<!-- Modal -->
<div class="modal fade text-left col-md" id="modal_logs_assign" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success white">
        <h4 class="modal-title white" id="myModal">
            <i class="la la-book"></i> Logs Pengiriman Pesan ke Teknisi</span>
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><i class="la la-lightbulb-o"></i> Informasi</h5>
        <p>Berikut merupakan daftar pesan yang telah dikirimkan kapada teknisi kepada pengguna yang bersangkutan</p>
        <hr>
        <div class="control-group">
        	<table class="table table-striped table-bordered column-visibility datatable" id="logs_assign">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Teknisi</th>
                  <th>Tanggal / Waktu Kunjungan</th>
                  <!-- <th>Pesan Terkirim</th> -->
                  <th>Pesan Assign</th>
                </tr>
              </thead>
              <tbody>
                <tr class="center">
                  <td colspan="4">Mengambil data...</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
          Tutup
        </button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      //show_logs_assign(4,"Sampel");
  })
  function show_logs_assign($id) {
    $tmp_id_pelanggan = $id;
    $("#modal_logs_assign").modal("show");
    get_data_assign();
  }
  function get_data_assign() {
    if ($.fn.DataTable.isDataTable('#logs_assign') ) {
      $('#logs_assign').DataTable().destroy();
    }
  	var daftar_pertanyaan = $('#logs_assign').DataTable({
        "responsive": true,
        "language": {searchPlaceholder: "Pertanyaan"},
        "ajax": {
          "url" : "{{Route('telegram_message_api.index')}}/"+$tmp_id_pelanggan,
          "headers": {
            "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
          },
        },
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "id_pesan_kunjungan", render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }, searchable: false, orderable: false},
            { "data": "teknisi.name"},
            { "data": "tanggal_kunjungan", render: function (data, type, row, meta) {
              return data+" / "+row['waktu_kunjungan'];
            }, searchable:false, orderable:false},
            /*{ "data": "created_at", searchable:false},*/
            { "data": "pesan_assign", orderable: false},
        ],
        "order": [[1, 'asc']]
    });
  }
</script>
<!-- Modal -->