<!-- Modal -->
<div class="modal fade text-left" id="modal-assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
	  <div class="modal-content">
	    <div class="modal-header bg-primary white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-mail-forward"></i> Assign Teknisi</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <h5><i class="la la-lightbulb-o"></i> Tips dan Informasi</h5>
	      <p>Untuk mencantumkan <code>text dinamis</code> pada pesan yang akan dikirimkan (jika diubah secara manual), silahkan gunakan tag: <code>#greeting#</code>, <code>#nama_teknisi#</code>,<code>#tanggal_kunjungan#</code>,<code>#waktu_kunjungan#</code>,<code>#nama_pelanggan#</code>,<code>#innet_pelanggan#</code>, <code>#no_pelanggan#</code>, dan/atau <code>#kekurangan_pemasangan#</code>.</p>
	      <hr>
	      <div class="control-group">
	      	<label>Nama Pelanggan</label>
	      	<input type="text" name="" id="nama_pelanggan" class="form-control" disabled="disabled">
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Nomor Inet Pelanggan</label>
	      	<input type="text" name="" id="innet_pelanggan" class="form-control" disabled="disabled">
	      </div>
	      <hr>
	      <div class="control-group">
	      	<label for="id_teknisi">Teknisi</label>
	      	<select class="select2 form-control" id="id_teknisi">
	      		<option value="" selected="selected">Pilih Teknisi</option>
	      		@foreach($teknisi as $val)
	      		<option value="{{$val->id_teknisi}}" selected="selected">{{$val->id_telegram}} | {{$val->name}}</option>
	      		@endforeach
	      	</select>
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Tanggal Kunjungan</label>
	      	<input type="date" name="" id="tanggal_kunjungan" class="form-control">
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Waktu Kunjungan</label>
	      	<input type="time" name="" id="waktu_kunjungan" class="form-control">
	      </div>
	      <hr>
	      <div class="control-group">
	      	<label>Pesan ke Teknisi</label>
	      	<textarea name="" id="pesan_assign" class="form-control" rows="5" placeholder="Opsional">Selamat #greeting# Teknisi #nama_teknisi#, pelanggan dengan no.inet #innet_pelanggan# atas nama #nama_pelanggan# kami informasikan bahwa terdapat perangkat MyIndihome #kekurangan_pemasangan# belum lengkap. Mohon segera di progres untuk melengkapinya dengan jadwal kunjungan ke pelanggan pada tanggal #tanggal_kunjungan# (#waktu_kunjungan#). Hubungi nomor pelanggan berikut: #no_pelanggan# untuk koordinasi. Terima Kasih</textarea>
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Batal/Tutup</button>
	      <button type="button" class="btn btn-primary" onclick="assign_teknisi()"><i class="la la-mail-forward"></i> Assign Teknisi</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".select2").select2({
	      placeholder: "Pilih Teknisi",
	      dropdownParent: $("#modal-assign"),
	      //allowClear: false,
	      width: '100%'
	    });
		//showAssign();
	})

	$tmp_id_pelanggan = 0;

	function showAssign($nama_pelanggan,$innet_pelanggan) {
		$("#nama_pelanggan").val($nama_pelanggan);
		$("#innet_pelanggan").val($innet_pelanggan);
		$("#id_teknisi").val("");
		$("#waktu_kunjungan").val("");
		$("#tanggal_kunjungan").val("");
		$("#pesan_assign").val("Selamat #greeting# Teknisi #nama_teknisi#, pelanggan dengan no.inet #innet_pelanggan# atas nama #nama_pelanggan# kami informasikan bahwa terdapat perangkat MyIndihome #kekurangan_pemasangan# belum lengkap. Mohon segera di progres untuk melengkapinya dengan jadwal kunjungan ke pelanggan pada tanggal #tanggal_kunjungan# (#waktu_kunjungan#). Hubungi nomor pelanggan berikut: #no_pelanggan# untuk koordinasi. Terima Kasih");
		$("#modal-assign").modal("show");
		$tmp_id_pelanggan = $innet_pelanggan;
	}

	function assign_teknisi() {
		$id_teknisi = $("#id_teknisi").val();
		$tanggal_kunjungan = $("#tanggal_kunjungan").val();
		$waktu_kunjungan = $("#waktu_kunjungan").val();
		$pesan_assign = $("#pesan_assign").val();
		if(!handle_select($id_teknisi,"Teknisi")) return;
		if(!handle_require($tanggal_kunjungan,"Tanggal kunjungan")) return;
		if(!handle_require($waktu_kunjungan,"Waktu kunjungan")) return;
		if(!handle_require($pesan_assign,"Pesan assign")) return;
		showLoading();
		$.ajax({
	  		url : "{{Route('telegram_message_api.index')}}",
	  		method: "POST",
	  		headers: {
	  			"token" : "{{Crypt::encrypt(auth::user()->id)}}",
	  		},
	      data: {
	        "id_pelanggan" : $tmp_id_pelanggan,
	        "id_teknisi" : $id_teknisi,
	        "tanggal_kunjungan" : $tanggal_kunjungan,
	        "waktu_kunjungan" : $waktu_kunjungan,
	        "pesan_assign" : $pesan_assign,
	      },
	  		success: function(res) {
	  			hideLoading();
	  			if(res.success) {
	  				miniNotif("success",res.pesan);
	  				$("#modal-assign").modal("hide");
	  				@php
	  					echo "target_link=\"".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."\";";
	  				@endphp
	  				//redirect(target_link,3000);
	  			} else
	  				miniNotif("error",res.pesan);
	  		},
	  		error: function(res) {
	  			hideLoading();
	  			miniNotif("error","Tidak dapat terhubung ke server!");
	  		},
	  	})
	}
</script>