<!-- Modal Info Alumni -->
<div class="modal fade text-left" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
	  <div class="modal-content">
	    <div class="modal-header bg-success white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-user"></i> <span class="nama_alumni">Ahmad Saparudin</span></h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <section id="basic-form-layouts">
          <div class="row match-height">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="basic-layout-form">Informasi Alumni</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <div class="card-text">
                      <p><i class="la la-lightbulb-o"></i> Untuk <code>menghubungi alumni</code> silahkan gunakan sistem informasi "{{env("APP_NAME","UNJA - Alumni Tracker")}} Mobile".</p>
                    </div>
                    <form class="form">
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Biodata Pribadi</h4>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Nama Lengkap</label>
                              <input type="text" id="nama_alumni" class="form-control" value="Ahmad Saparudin" disabled="disabled">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput2">NIM</label>
                              <input type="text" id="nim_alumni" class="form-control" value="F1E116030" disabled="disabled">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput3">Program Studi</label>
                              <input type="text" id="prodi_alumni" class="form-control" value="Sistem Informasi" disabled="disabled">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput4">Fakultas</label>
                              <input type="text" id="fakultas_alumni" class="form-control" value="Sains dan Tekologi" disabled="disabled">
                            </div>
                          </div>
                        </div>
                        <h4 class="form-section"><i class="la la-graduation-cap"></i> Pendidikan Lanjutan</h4>
                        <div class="form-group">
                        	<table class="table" id="tabel-jenjang-lanjutan">
                        		<thead>
                        			<tr>
                        				<th>No</th>
                        				<th>Instansi</th>
                        				<th>Jenjang</th>
                        				<th>Studi Linier</th>
                        				<th>Status</th>
                        			</tr>
                        		</thead>
                        	</table>

                        	<tbody>
                        	</tbody>
                        </div>
                        <h4 class="form-section"><i class="la la-black-tie"></i> Riwayat Pekerjaan</h4>
                        <div class="form-group">
                        	<table class="table" id="tabel-riwayat-pekerjaan">
                        		<thead>
                        			<tr>
                        				<th>No</th>
                        				<th>Perusahaan</th>
                        				<th>Profesi</th>
                        				<th>Gaji</th>
                        			</tr>
                        		</thead>
                        	</table>
                        	<tbody>
                        	</tbody>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//infoAlumni("12184");
	});
	function infoAlumni($id) {
		showLoading();
		$.ajax({
			url : "{{Route('alumni.index')}}/"+$id,
			headers: {
				"token" : "{{Crypt::encrypt(Auth::user()->id)}}",
			},
			success: function(res) {
				hideLoading();
				if(res.success) {
					printInfo(res.data);
					$("#modal-detail").modal("show");
				} else
					miniNotif("error",res.pesan);
			},
			error: function() {
				hideLoading();
				miniNotif("error","Tidak dapat terhubung ke server!");
			}
		})
	}
	function printInfo($data) {
		// Cetak Data Siakad
		$(".nama_alumni").html($data.siakad.name);
		$("#nama_alumni").val($data.siakad.name);
		$("#nim_alumni").val($data.siakad.username);
		$("#prodi_alumni").val($data.siakad.mahasiswa.mhs_pt.prodi.nama_prodi);
		$("#fakultas_alumni").val($data.siakad.mahasiswa.mhs_pt.prodi.fakultas.nama_fakultas);
		// generate dataset
		$pendidikan_lanjutan = [];
		for($i=0;$i<$data.pendidikan_lanjutan.length;$i++) {
			// no - instansi - jenjang - studi linier - status
			$no = $i+1;
			$instansi = $data.pendidikan_lanjutan[$i].instansi.nama_instansi;
			$jenjang  = $data.pendidikan_lanjutan[$i].jenjang_pendidikan.nama_jenjang;
			$studiLinier = "Linier";
			if(!$data.pendidikan_lanjutan[$i].studiLinier) $studiLinier = "Tidak Linier";
			$status = "Sedang Ditempuh";
			if(!$data.pendidikan_lanjutan[$i].hasGraduate) $status = "Selesai";
			// generate record
			$record = [$no,$instansi,$jenjang,$studiLinier,$status];
			// put to dataset
			$pendidikan_lanjutan.push($record);
		}
		$("#tabel-jenjang-lanjutan").dataTable().fnDestroy();
		$("#tabel-jenjang-lanjutan").DataTable({
			"data"   : $pendidikan_lanjutan,
		});
		// generate dataset riwayat pekerjaan
		$riwayat_pekerjaan = [];
		for($i=0;$i<$data.riwayat_kerja.length;$i++) {
			// no - perusahaan - profesi - gaji
			$no = $i+1;
			$perusahaan  = $data.riwayat_kerja[$i].tempat_kerja.nama_perusahaan;
			$profesi     = $data.riwayat_kerja[$i].workAS;
			$gaji_money  = $data.riwayat_kerja[$i].gaji;
			$gaji_satuan = $data.riwayat_kerja[$i].satuan_gaji.satuan;
			// generate record
			$record = [$no,$perusahaan,$profesi,("Rp"+$gaji_money+"/"+$gaji_satuan)];
			// put to dataset
			$riwayat_pekerjaan.push($record);
		}
		$("#tabel-riwayat-pekerjaan").dataTable().fnDestroy();
		$("#tabel-riwayat-pekerjaan").DataTable({
			"data"   : $riwayat_pekerjaan,
		});
		$(".table").css("width","100%");
	}
</script>