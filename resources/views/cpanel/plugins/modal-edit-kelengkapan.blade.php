<!-- Modal -->
<div class="modal fade text-left" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
	  <div class="modal-content">
	    <div class="modal-header bg-info white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-edit"></i> Edit Kelengkapan</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <h5><i class="la la-lightbulb-o"></i> Tips dan Informasi</h5>
	      <p>Mohon hanya ubah kelengkapan, jika layanan yang bersangkutan memang telah dipasang atau justru belum dipasang.</p>
	      <hr>
	      <div class="control-group">
	      	<label>Nama Pelanggan</label>
	      	<input type="text" name="" id="nama_pelanggan_edit" class="form-control" disabled="disabled">
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Nomor Inet Pelanggan</label>
	      	<input type="text" name="" id="innet_pelanggan_edit" class="form-control" disabled="disabled">
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>UseeTV</label>
	      	<select class="form-control" id="layanan_useetv">
	      		<option value="null">Tidak Dipasang</option>
	      		<option value="0">Belum Dipasang</option>
	      		<option value="1">Sudah Dipasang</option>
	      	</select>
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Inet</label>
	      	<select class="form-control" id="layanan_inet">
	      		<option value="null">Tidak Dipasang</option>
	      		<option value="0">Belum Dipasang</option>
	      		<option value="1">Sudah Dipasang</option>
	      	</select>
	      </div>
	      <br>
	      <div class="control-group">
	      	<label>Voice</label>
	      	<select class="form-control" id="layanan_voice">
	      		<option value="null">Tidak Dipasang</option>
	      		<option value="0">Belum Dipasang</option>
	      		<option value="1">Sudah Dipasang</option>
	      	</select>
	      </div>
	      <hr>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
	      <button type="button" class="btn btn-info" onclick="edit_kelengkapan()"><i class="la la-edit"></i> Edit Kelengkapan</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//$("#modal-edit").modal("show");
		//show_edit_kelengkapan('2147483656');
	})

	$tmp_id_pelanggan = 0;

	function get_all_val() {
		$res = "";
		$res+= '<option value="null">Tidak Dipasang</option>';
		$res+= '<option value="0">Belum Dipasang</option>';
		$res+= '<option value="1">Sudah Dipasang</option>';
		return $res;
	}

	function get_normal_value() {
		$res = "";
		$res+= '<option value="0">Belum Dipasang</option>';
		$res+= '<option value="1">Sudah Dipasang</option>';
		return $res;
	}

	function set_default_val() {
		$("#layanan_useetv").val("null");
		$("#layanan_inet").val("null");
		$("#layanan_voice").val("null");
	}

	function set_default_layanan() {
		$("#layanan_useetv").html(get_all_val());
		$("#layanan_inet").html(get_all_val());
		$("#layanan_voice").html(get_all_val());
	}

	function disable_layanan() {
		$("#layanan_useetv").attr("disabled","disabled");
		$("#layanan_inet").attr("disabled","disabled");
		$("#layanan_voice").attr("disabled","disabled");
	}

	function enable_layanan($mode="all") {
		if($mode=="all") {
			$("#layanan_useetv").removeAttr("disabled");
			$("#layanan_inet").removeAttr("disabled");
			$("#layanan_voice").removeAttr("disabled");
		} else {
			$("#layanan_"+$mode).removeAttr("disabled");
		};
	}

	function handle_installed($kelengkapan,$target) {
		if($kelengkapan)
			$("#layanan_"+$target).val("1")
		else
			$("#layanan_"+$target).val("0");
	}

	function show_edit_kelengkapan($innet_pelanggan) {
		set_default_layanan();
		disable_layanan();
		$tmp_id_pelanggan = $innet_pelanggan;
		showLoading();
		$.ajax({
			url : "{{Route('data_master')}}/get_detail_kelengkapan/"+$tmp_id_pelanggan,
	  		method: "GET",
	  		headers: {
	  			"token" : "{{Crypt::encrypt(auth::user()->id)}}",
	  		},
	  		success: function(res) {
	  			hideLoading();
	  			if(res.success) {
	  				$data = res.data;
	  				$("#nama_pelanggan_edit").val($data.nama_pelanggan);
	  				$("#innet_pelanggan_edit").val($data.id_pelanggan);
	  				// Generate select
	  				console.log($data.must_pasang.inet);
	  				if($data.must_pasang.inet) {
	  					$("#layanan_inet").html(get_normal_value());
	  					enable_layanan("inet");
	  					handle_installed($data.kelengkapan.inet,"inet");
	  				}
	  				if($data.must_pasang.voice) {
	  					$("#layanan_voice").html(get_normal_value());
	  					enable_layanan("voice");
	  					handle_installed($data.kelengkapan.voice,"voice");
	  				}
	  				if($data.must_pasang.useetv) {
	  					$("#layanan_useetv").html(get_normal_value());
	  					enable_layanan("useetv");
	  					handle_installed($data.kelengkapan.useetv,"useetv");
	  				}
	  				// Show modal
	  				$("#modal-edit").modal("show");
	  			} else
	  				miniNotif("error",res.pesan);
	  		},
	  		error: function() {
	  			miniNotif("error","Tidak dapat terhubung ke server!");
	  			hideLoading();
	  		}
		});
	}

	function set_icon($active,$target) {
		$complete_image = "<img src=\"{{asset('img/icons/mini/check.png')}}\">";
		$notcomplete_image = "<img src=\"{{asset('img/icons/mini/times.png')}}\">";
		if($active==1) {
			$("#status"+"-"+$target+"-"+$tmp_id_pelanggan).html($complete_image);
		} else if($active==0) {
			$("#status"+"-"+$target+"-"+$tmp_id_pelanggan).html($notcomplete_image);
		}
	}

	function edit_kelengkapan() {
		$useetv = $("#layanan_useetv").val();
		$inet = $("#layanan_inet").val();
		$voice = $("#layanan_voice").val();
		showLoading();
		$.ajax({
	  		url : "{{Route('kelengkapan.index')}}/"+$tmp_id_pelanggan,
	  		method: "PUT",
	  		headers: {
	  			"token" : "{{Crypt::encrypt(auth::user()->id)}}",
	  		},
	      data: {
	        "useetv" : $useetv,
	        "inet" : $inet,
	        "voice" : $voice,
	      },
	  		success: function(res) {
	  			hideLoading();
	  			if(res.success) {
	  				// Update icon
	  				set_icon($useetv,"useetv");
	  				set_icon($inet,"inet");
	  				set_icon($voice,"voice");
	  				if(res.complete)
	  					$("#status-"+$tmp_id_pelanggan).html("Lengkap");
	  				else
	  					$("#status-"+$tmp_id_pelanggan).html("Belum Lengkap");
	  				// Show notif + hide modal
	  				miniNotif("success",res.pesan);
	  				$("#modal-edit").modal("hide");
	  			} else
	  				miniNotif("error",res.pesan);
	  		},
	  		error: function(res) {
	  			hideLoading();
	  			miniNotif("error","Tidak dapat terhubung ke server!");
	  		},
	  	})
	}
</script>