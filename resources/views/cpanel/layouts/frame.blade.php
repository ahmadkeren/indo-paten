<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  @include("cpanel.layouts.default-header-links")
  @yield("plugin")
</head>
<body class="vertical-layout vertical-menu 1-column  bg-cyan bg-lighten-2 menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="1-column">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark navbar-shadow border-bottom-unja">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto">
            <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
              <i class="ft-menu font-large-1"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="navbar-brand" href="{{(Route('landing'))}}">
              <img class="brand-logo" alt="Logo" src="{{asset('app-assets/images/logo/logo.png')}}">
              <h3 class="brand-text">{{env("APP_TITLE","Ahmad App")}}</h3>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container">
        <div class="collapse navbar-collapse justify-content-end" id="navbar-mobile">
          <ul class="nav navbar-nav">
            @include("cpanel.layouts.navigasi-frame")
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="app-content content" style="background-image:url('{{asset('img/bg-landing.png')}}');background-color:#fcdbb7">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
          @yield("konten")
      </div>
  </div>
</div>
  <footer class="footer fixed-bottom footer-dark navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">&copy {{env('APP_COPY', 'Ahmad Saparudin, 2019')}}</span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"> Didedikasikan untuk {{env('APP_DISTRIBUTED_FOR', 'Universitas Jambi')}}</span>
    </p>
  </footer>
  @include("cpanel.layouts.default-scripts")
  @yield("script")
</body>
</html>