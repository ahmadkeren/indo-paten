<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-telkom navbar-shadow">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item">
          <a class="navbar-brand" href="#">
            <img class="brand-logo" alt="Logo" src="{{asset('app-assets/images/logo/logo.png')}}" style="width:150px">
          </a>
        </li>
        <li class="nav-item d-md-none">
          <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
        </li>
      </ul>
    </div>
    <div class="navbar-container content">
      <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="nav navbar-nav mr-auto float-left">
          <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
          <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
        </ul>
        <ul class="nav navbar-nav float-right">
          <li class="dropdown dropdown-user nav-item">
            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <span class="mr-1">Selamat Datang,
                <span class="user-name text-bold-700">{{Auth::user()->name}}</span>
              </span>
              <span class="avatar avatar-online">
                @if(Auth::user()->user->photo==null)
                <img src="{{asset('img/avatar.jpg')}}" alt="{{Auth::user()->username}}">
                @else
                <img src="data::image/jpeg;base64, {{Auth::user()->user->photo}}" alt="{{Auth::user()->username}}">
                @endif
                <i></i>
              </span>
            </a>
            <form action="{{url(Route('logout'))}}" method="post" style="display:none" id="logout">
              {{csrf_field()}}
            </form>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{url(Route('profil_edit'))}}">
                <i class="ft-user"></i> Update Profil
              </a>
              <a class="dropdown-item" href="{{url(Route('profil_password'))}}">
                <i class="ft-lock"></i> Ganti Sandi
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout').submit();"><i class="ft-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>