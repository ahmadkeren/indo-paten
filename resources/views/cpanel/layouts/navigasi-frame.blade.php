<li class="nav-item">
  <a class="nav-link mr-2 nav-link-label" href="{{Route('landing')}}">
    <i class="la la-home"></i> Beranda
  </a>
</li>
@if(Auth::guest())
<li class="nav-item">
  <a class="nav-link mr-2 nav-link-label" href="login">
    <i class="la la-sign-in"></i> Login
  </a>
</li>
@else
<li class="nav-item">
  <a class="nav-link mr-2 nav-link-label" href="{{Route('home')}}">
    <i class="la la-dashboard"></i> Dashboard
  </a>
</li>
@endif