<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins/auth/css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins/auth/css/sweetalert2.css')}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
  <script type="text/javascript" src="{{asset('app-assets-custom\plugins/auth/js/sweetalert2.js')}}"></script>
  <script type="text/javascript" src="{{asset('app-assets-custom\plugins/auth/js/custom.js')}}"></script>
  <script type="text/javascript" src="{{asset('app-assets-custom\plugins/auth/js/inputan.js')}}"></script>
  <script type="text/javascript" src="{{asset('app-assets/js/core/libraries/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('app-assets-custom/js/default.js')}}"></script>
  <!-- Plugins Loading -->
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/extensions/toastr.css')}}">
  <script src="{{asset('app-assets-custom/js/loadingoverlay.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('app-assets-custom/js/loadingoverlay-set.js')}}" type="text/javascript"></script>
  <!-- Plugins Toastr -->
  <script src="{{asset('app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('app-assets-custom/js/toastr-set.js')}}" type="text/javascript"></script>
  @yield("plugin")
</head>
<body style="background-image:url('{{asset('img/bg_abstrak.jpg')}}');background-size:100% 100%;">
<!-- <body> -->
  <div class="wrapper fadeInDown">
    <div style="position:absolute;left:10%;text-align:center;">
      <div class="logo" style="padding-bottom:10px">
        <img src="{{asset('img/logo-ipass-black.png')}}" style="height:150px;">
      </div>
      <div id="formContent">
        @yield("status")
        <div class="fadeIn first">
          <!-- <img src="{{asset('app-assets-custom\plugins/auth/gambar/icon.svg')}}" id="icon" alt="User Icon" /> -->
        </div>
        @yield("konten")
        <div id="formFooter">
            <div class="col-md-6" style="float:left;">
              <a href="#" class="white">Register</a>
            </div>
            <div class="col-md-6" style="float:right;">
              <a href="#" class="white">Forgot Your Password?</a>
            </div>
            <br>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      @if (session('status'))
          notif("success","Link untuk mereset kata sandi berhasil dikirim ke email Anda!");
      @elseif ($errors->has('username'))
          notif("error","Akun tidak ditemukan pada sistem! <br>Gunakan akun siakad untuk login!");
      @elseif ($errors->has('password'))
          notif("error","{{$errors->first('password')}}");
      @endif
    </script>
</body>
</html>