<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="{{ env('APP_DESCRIPTION', '-') }}">
<meta name="keywords" content="{{ env('APP_KEYWORDS') }}">
<meta name="author" content="{{ env('APP_AUTHOR', 'Ahmad Saparudin') }}">
<title>{{ config('app.name', 'Ahmad Saparudin') }}</title>
<link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/fonts/google/css/open-sans.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/fonts/line-awesome/css/line-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/vendors.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/app.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-gradient.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/cryptocoins/cryptocoins.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom/css/style.css')}}">
<script type="text/javascript" src="{{asset('app-assets/js/core/libraries/jquery.min.js')}}"></script>
<!-- Plugin Toastr -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/extensions/toastr.css')}}">