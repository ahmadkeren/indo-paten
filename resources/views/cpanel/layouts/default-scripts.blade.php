<script src="{{asset('app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
<!-- Plugins Loading -->
<script src="{{asset('app-assets-custom/js/loadingoverlay.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets-custom/js/loadingoverlay-set.js')}}" type="text/javascript"></script>
<!-- Plugins Toastr -->
<script src="{{asset('app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets-custom/js/toastr-set.js')}}" type="text/javascript"></script>
<!-- Custom Script -->
<script src="{{asset('app-assets-custom/js/default.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('app-assets-custom/js/menu-set.js')}}"></script>
<script type="text/javascript" src="{{asset('app-assets-custom/js/validate-set.js')}}"></script>