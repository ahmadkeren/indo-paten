@extends("cpanel.layouts.auth")

@section("konten")
<form action="#" autocomplete="off" onsubmit="return false">
    <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username" value="{{old('username')}}" onkeypress="return validasi_username(event)" required autocomplete="off">
    <input type="password" id="password" class="fadeIn third" name="password" placeholder="Kata Sandi" required autocomplete="off">
    <input type="submit" class="fadeIn fourth" value="Masuk" onclick="doLogin()" id="btn-login">
</form>
@endsection

@section("status")
<a href="#"><h2 class="active"></h2></a>
@endsection

@section("plugin")
<script type="text/javascript">
    $tryLogin = 0;
  function handleLogin() {
      $tryLogin++;
      if($tryLogin == 3) {
          $("#username").val("");
          $("#password").val("");
          $tryLogin = 0;
      };
  }
  function disableLogin() {
    $("#username").attr("disabled","disabled");
    $("#password").attr("disabled","disabled");
    $("#btn-login").attr("disabled","disabled");
  }
  function doLogin() {
    $username = $("#username").val();
    $password = $("#password").val();
    if($username.trim().length==0)
      miniNotif("warning","Username belum diinput"); else
    if($password.trim().length==0)
      miniNotif("warning","Password belum diinput"); else
    {
      showLoading();
      $.ajax({
          url    : "{{Route('login')}}",
          method : "POST",
          data   : {
              "username" : $username,
              "password" : $password,
          },
          success:  function(res) {
              hideLoading();
              if(res.success) {
                  miniNotif("success",res.pesan);
                  disableLogin();
                  redirect("{{Route('home')}}",3000);
              } else {
                  miniNotif("error",res.pesan);
                  handleLogin();
              };
          },
          erorr: function() {
              hideLoading();
              miniNotif("erorr","Tidak dapat terhubung ke server! Silahkan periksa koneksi internet!");
          }
      });
    }
  }
</script>
@endsection