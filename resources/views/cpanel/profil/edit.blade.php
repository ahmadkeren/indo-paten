@extends("cpanel.layouts.app")

@section("konten")
<div class="content-body">
<div id="user-profile">
  <div class="row">
    <div class="col-12">
      <div class="card profile-with-cover">
        <div class="card-img-top img-fluid bg-cover height-300" style="background: url('{{asset('img/foto-sampul.jpg')}}') 50%;"></div>
        <div class="media profil-cover-details w-100">
          <div class="media-left pl-2 pt-2">
            <a href="#" class="profile-image" onclick="ganti_foto()">
            	@if(Auth::user()->user->photo==null)
            	<img src="{{asset('img/avatar.jpg')}}" class="rounded-circle img-border height-100" alt="{{Auth::user()->username}}">
            	@else
            	<img src="data::image/jpeg;base64, {{Auth::user()->user->photo}}" class="rounded-circle img-border height-100" alt="{{Auth::user()->username}}">
            	@endif
            </a>
          </div>
          <div class="media-body pt-3 px-2">
            <div class="row">
              <div class="col">
                <h3 class="card-title">{{Auth::user()->name}}</h3>
              </div>
              <div class="col text-right">
                <div class="btn-group d-none d-md-block float-right ml-2" role="group" aria-label="Basic example">
                	<form action="#" method="POST" id="upload_foto" style="display:none">
                		<input type="file" name="photo" onchange="upload_foto()" accept="image/*">
                	</form>
                	<button type="button" class="btn btn-success" onclick="ganti_foto()">
                		<i class="la la-refresh"></i> Ganti Foto Profil
                	</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-light navbar-profile align-self-end">
          <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false"
          aria-label="Toggle navigation"></button>
          <nav class="navbar navbar-expand-lg">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link capitalize" href="#">
                  	<i class="la la-user"></i> 
                  	{{auth::user()->usertype}}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link capitalize" href="#">
                  	<i class="la la-file-text"></i> 
                  	{{auth::user()->username}}
                  </a>
                </li>
              </ul>
            </div>
      </div>
      </nav>
    </div>
  </div>
</div>
<!-- Bagian Konten -->
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title" id="bordered-layout-colored-controls">Profil Saya</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collpase show">
          <div class="card-body">
            <div class="card-text">
            </div>
            <form class="form form-horizontal form-bordered" action="#" onsubmit="return false" method="POST">
              <div class="form-body">
                <h4 class="form-section"><i class="la la-user"></i> Akun Saya</h4>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="userinput1">Nama</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control border-primary" value="{{Auth::user()->name}}" disabled="disabled">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="userinput2">Email</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control border-primary" value="{{Auth::user()->email}}" disabled="disabled">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$current_ponsel   = "{{Auth::user()->user->ponsel}}";
	$current_whatsapp = "{{Auth::user()->user->whatsapp}}";
	$current_status   = "{{Auth::user()->user->status}}";
	function initiateForm() {
		$("#ponsel").val($current_ponsel);
		$("#whatsapp").val($current_whatsapp);
		$("#status").val($current_status);
	}
	$(document).ready(function(){
		initiateForm();
	});
	function update_profil() {
		$ponsel   = $("#ponsel").val();
		$whatsapp = $("#whatsapp").val();
		$status   = $("#status").val();
		showLoading();
		$.ajax({
			url    : "{{Route('user.store')}}",
			method : "POST",
			headers: {
				"token"    : "{{Crypt::encrypt(auth::user()->id)}}",
			},
			data   : {
				"ponsel"   : $ponsel,
				"whatsapp" : $whatsapp,
				"status"   : $status,
			},
			success: function(res) {
				hideLoading();
				if(res.success) {
					$current_ponsel   = $ponsel;
					$current_whatsapp = $whatsapp;
					$current_status   = $status;
					miniNotif("success",res.pesan);
				} else
					miniNotif("error",res.pesan);
			},
			error  : function() {
				hideLoading();
				miniNotif("error","Tidak dapat terhubung ke server!");
			}
		})
	}
	function ganti_foto() {
		$("#upload_foto input").click();
	}
	function upload_foto() {
		showLoading();
		$file_data = $("#upload_foto input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('photo',$file_data);
		$.ajax({
			url    : "{{Route('user.store')}}",
			method : "POST",
			cache  : false,
			contentType: false,
			processData: false,
			headers: {
				"token"    : "{{Crypt::encrypt(auth::user()->id)}}",
			},
			data   : $form_data,
			success: function(res) {
				hideLoading();
				if(res.success) {
					miniNotif("success",res.pesan);
					redirect("{{Route('profil_edit')}}");
				} else
					miniNotif("error",res.pesan);
			},
			error  : function() {
				hideLoading();
				miniNotif("error","Tidak dapat terhubung ke server!");
			}
		})
	}
</script>
@endsection

@section("script")
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}"">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-gradient.css')}}"">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/users.css')}}"">
@endsection