@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Ganti Kata Sandi</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a>
        </li>
        <li class="breadcrumb-item"><a href="#">Akun Saya</a>
        </li>
        <li class="breadcrumb-item active">Ganti Kata Sandi
        </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title" id="striped-row-layout-icons">Form Update Password</h4>
        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            <li><a data-action="close"><i class="ft-x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collpase show">
        <div class="card-body">
          <form class="form form-horizontal form-bordered" action="#" onsubmit="return false" method="POST">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class="col-md-3 label-control">Kata Sandi</label>
                      <div class="col-md-9">
                        <input type="password" class="form-control border-primary" placeholder="Input Kata Sandi Saat Ini" id="password_current">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 label-control">Kata Sandi Baru</label>
                      <div class="col-md-9">
                        <input type="password" class="form-control border-primary" placeholder="Input Kata Sandi Baru" id="password">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3 label-control">Konfirmasi Kata Sandi Baru</label>
                      <div class="col-md-9">
                        <input type="password" class="form-control border-primary" placeholder="Ketik ulang kata sandi baru" id="password_confirm">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <button type="button" class="btn btn-warning mr-1" onclick="initiateForm()">
                  <i class="la la-close"></i> Batal Perubahan
                </button>
                <button type="button" class="btn btn-primary" onclick="update_profil()">
                  <i class="la la-check-square-o"></i> Simpan Perubahan
                </button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function initiateForm() {
    $("#password").val("");
    $("#password_current").val("");
    $("#password_confirm").val("");
  }
  $(document).ready(function(){
    initiateForm();
  });
  function update_profil() {
    $password           = $("#password").val();
    $password_current   = $("#password_current").val();
    $password_confirm   = $("#password_confirm").val();
    // sistem validasi
    $block_action = false;
    if(
        $password.trim().length==0 || 
        $password_current.trim().length==0 ||
        $password_confirm.trim().length==0
      ){
      miniNotif("error","Inputan belum lengkap!");
      $block_action = true;
    } else
    if($password.length<8){
      miniNotif("error","Kata sandi paling tidak terdiri dari 8 karakter!");
      $block_action = true;
    } else
    if($password != $password_confirm){
      miniNotif("error","Konfirmasi kata sandi tidak sama!");
      $block_action = true;
    }
    // Cek validasi
    if($block_action) return;
    showLoading();
    $.ajax({
      url    : "{{Route('user.store')}}",
      method : "POST",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      data   : {
        "password"           : $password,
        "password_confirm"   : $password_confirm,
        "password_current"   : $password_current,
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          miniNotif("success",res.pesan);
          initiateForm();
        } else
          miniNotif("error",res.pesan);
      },
      error  : function() {
        hideLoading();
        miniNotif("error","Tidak dapat terhubung ke server!");
      }
    })
  }
</script>
@endsection