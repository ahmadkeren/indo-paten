<title>Print Preview</title>
<style type="text/css">
	table {
		border-collapse: collapse;
	}
	tr, td, th {
		border:2px solid;
	}
  th {
    text-align: center;
  }
	.center {
		text-align: center;
	}
	.right {
		text-align: right;
	}
</style>

<center><h1>LAPORAN PELANGGAN</h1></center>
<table class="table table-striped table-bordered column-visibility datatable" id="result_table" style="width:100%">
  <thead>
    <tr>
      <th rowspan="2">No</th>
      <th rowspan="2">Nama Pelanggan</th>
      <th rowspan="2">Jenis Layanan</th>
      <th rowspan="2">No. Inet</th>
      <th rowspan="2">Tanggal PS</th>
      <th colspan="3">Kelengkapan Perangkat</th>
      <th rowspan="2">Status</th>
    </tr>
    <tr>
      <th>UseeTV</th>
      <th>Inet</th>
      <th>Voice</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $index => $value)
    <tr>
      <td class="center">{{$index+1}}</td>
      <td>{{$value->nama_pelanggan}}</td>
      <td>{{$value->layanan->type}} ({{$value->layanan->services}})</td>
      <td class="center">{{$value->id_pelanggan}}</td>
      <td class="center">{{$value->tanggal_pasang}}</td>
      <td class="center">
        @if($value['kelengkapan']['useetv'])
        <img src="{{public_path('img/icons/mini/check.png')}}">
        @else
          @if($value['must_pasang']['useetv'])
          <img src="{{public_path('img/icons/mini/times.png')}}">
          @else
          <i class="la la-minus"></i>
          @endif
        @endif
      </td>
      <td class="center">
        @if($value['kelengkapan']['inet'])
        <img src="{{public_path('img/icons/mini/check.png')}}">
        @else
          @if($value['must_pasang']['inet'])
          <img src="{{public_path('img/icons/mini/times.png')}}">
          @else
          <i class="la la-minus"></i>
          @endif
        @endif
      </td>
      <td class="center">
        @if($value['kelengkapan']['voice'])
        <img src="{{public_path('img/icons/mini/check.png')}}">
        @else
          @if($value['must_pasang']['voice'])
          <img src="{{public_path('img/icons/mini/times.png')}}">
          @else
          <i class="la la-minus"></i>
          @endif
        @endif
      </td>
      <td class="center">
        @if($value['lengkap'])
        Lengkap
        @else
        Belum Lengkap
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

<p class="right">
	Dicetak pada {{date('H:i:s d-m-Y')}} oleh {{Auth::user()->name}}
</p>