@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Pengaturan User Role</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a>
        </li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a>
        </li>
        <li class="breadcrumb-item active">User Role
        </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Pengguna Terdaftar</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <table class="table table-striped table-bordered column-visibility" id="my_table">
              <thead>
                <tr>
                  <th>Nama Lengkap</th>
                  <th>Alamat Email</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr class="center">
                  <td colspan="4">Mengambil data...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
      var datatable = $('#my_table').DataTable({
        "language": {searchPlaceholder: "Nama / Username"},
        "ajax": "{{Route('get_user')}}",
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "id", render: function (data, type, row, meta) {
                return "<button class='btn btn-sm btn-primary' onclick='manageRole("+data+",\""+row['email']+"\")'><i class='la la-cog'></i> Manage Role</button>";
            }, "searchable": false, "orderable": false},
        ],
        "order": [[1, 'asc']]
    });
  });
</script>

<!-- Modal -->
<div class="modal fade text-left col-md" id="modal_utama" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success white">
        <h4 class="modal-title white" id="myModal">
            <i class="la la-cog"></i> Manajemen User Role
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><i class="la la-lightbulb-o"></i> Tips dan Informasi</h5>
        <p>Tarik role dari kolom sebelah kiri ke sebelah kanan untuk menambahkan role kepada pengguna. Sebaliknya, untuk menghapus tarik role dari sebelah kanan ke sebelah kiri.</p>
        <hr>
        <h5><i class="la la-info-circle"></i> User Information</h5>
        <p>Penetapan role akan ditetapkan kepada user: <span id="username">F1E116030</span>.</p>
        <hr>
        <div class="row">
            <!-- Basic List Group -->
            <div class="col-lg-6">
              <div class="mb-1">
                <h5 class="mb-0">Avaibale Role</h5>
                <small class="text-muted">Daftar role yang tersedia.</small>
              </div>
              <div class="card">
                <ul class="list-group rule-table padding-5" id="available-rule">
                </ul>
              </div>
            </div>
            <!-- List Group With Tags -->
            <div class="col-lg-6">
              <div class="mb-1">
                <h5 class="mb-0">Rule Pengguna</h5>
                <small class="text-muted">Daftar role yang dimiliki pengguna.</small>
              </div>
              <div class="card">
                <ul class="list-group rule-table padding-5" id="current-rule">
                </ul>
              </div>
            </div>
        </div>
        <div class="alert alert-success" role="alert">
          <span class="text-bold-600">Well done!</span> Klik tombol simpan dibawah untuk menerapkan perubahan role pengguna.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
          Tutup
        </button>
        <button type="button" class="btn btn-outline-success" onclick="saveRule()">
          Simpan Perubahan
        </button>
        <input type="hidden" name="" id="target_id">
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript">
  $(document).ready(function(){
      
  })
  function manageRole($id_user,$username) {
    $("#target_id").val($id_user);
    $("#username").html($username);
    getServerData($id_user);
  }
  function getServerData($id_user) {
      showLoading();
      $.ajax({
          url     : "{{Route('role.index')}}/"+$id_user,
          headers : {
              "token" : "{{Crypt::encrypt(Auth::user()->id)}}",
          },
          success : function(res) {
              hideLoading();
              if(res.success) {
                  printRole(res.data.available_role,"#available-rule");
                  printRole(res.data.user_role,"#current-rule");
                  redeclareList();
                  $("#modal_utama").modal("show");
              } else
                miniNotif("error",res.pesan);
          },
          error   : function() {
              hideLoading();
              miniNotif("error","Koneksi ke server bermasalah!");
          }
      })
  }
  $has_declare = false;
  function redeclareList() {
    if($has_declare) return;
      dragula(
        [
          document.getElementById("available-rule"),
          document.getElementById("current-rule")
        ]
      )
      $has_declare = true;
  }
  function saveRule() {
      $id_user  = $("#target_id").val();
      showLoading();
      $.ajax({
          url     : "{{Route('role.store')}}",
          method  : "POST",
          headers : {
              "token" : "{{Crypt::encrypt(Auth::user()->id)}}",
          },
          data    : {
              "id_user" : $id_user,
              "roles"   : getUserRole(),
          },
          success : function(res) {
            hideLoading();
            if(res.success) {
              miniNotif("success",res.pesan);
              $("#modal_utama").modal("hide");
            } else
              miniNotif("error",res.pesan);
          },
          error   : function() {
            hideLoading();
            miniNotif("error","Tidak dapat terhubung ke server!");
          }
      })
  }
  function getUserRole() {
      $role = [];
      $('#current-rule .list-group-item').each(function(){
          $id_role = $(this).attr("value");
          $role.push($id_role);
      });
      return $role;
  }
  function printRole($data,$target) {
      $role = "";
      for($i=0;$i<$data.length;$i++) {
        $role+='<li class="list-group-item" value="'+$data[$i].id+'">'+$data[$i].role_name+'</li>';
      };
      $($target).html($role);
  }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Drag And Drop -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/ui/dragula.min.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Drag And Drop -->
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/dragula.min.js')}}" type="text/javascript"></script>
@endsection