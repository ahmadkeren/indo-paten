@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Pengaturan Landing Page</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
        <li class="breadcrumb-item active">Landing Page</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title" id="striped-row-layout-icons">Form Update Landing Page</h4>
        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            <li><a data-action="close"><i class="ft-x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collpase show">
        <div class="card-body">
          <form class="form form-horizontal form-bordered" action="#" onsubmit="return false" method="POST">
          	  <h4 class="form-section"><i class="la la-file-o"></i> About US</h4>
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" id="about_us">{!!$data->about!!}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <h4 class="form-section"><i class="la la-file-o"></i> Blackquote</h4>
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" id="blackquote">{!!$data->blackquote_content!!}</textarea>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top:10px">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="projectinput1">Author Blackquote</label>
                      <input type="text" name="" maxlength="30" class="form-control" placeholder="Contoh: Ahmad" id="author" value="{!!$data->blackquote_author!!}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="projectinput1">Title Author</label>
                      <input type="text" name="" maxlength="30" class="form-control" placeholder="Contoh: CEO" id="author_title" value="{!!$data->blackquote_title!!}">
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <h4 class="form-section"><i class="la la-file-o"></i> Contact US</h4>
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" id="contact">{!!$data->contact!!}</textarea>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top:10px">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="projectinput1">Email</label>
                      <input type="email" name="" maxlength="30" class="form-control" placeholder="Contoh: circledeveloperid@gmail.com" id="email" value="{!!$data->email!!}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <a type="button" class="btn btn-warning mr-1" href="{{Route('set_landing')}}">
                  <i class="la la-close"></i> Batal Perubahan
                </a>
                <button type="button" class="btn btn-primary" onclick="update_profil()">
                  <i class="la la-check-square-o"></i> Simpan Perubahan
                </button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    CKEDITOR.replace("about_us",{removeButtons: 'Save,About',});
    CKEDITOR.replace("blackquote",{removeButtons: 'Save,About',});
    CKEDITOR.replace("contact",{removeButtons: 'Save,About',});
  });
  function update_profil() {
    $about             = CKEDITOR.instances['about_us'].getData();
    $blackquote        = CKEDITOR.instances['blackquote'].getData();
    $blackquote_author = $("#author").val();
    $blackquote_title  = $("#author_title").val();
    $contact           = CKEDITOR.instances['contact'].getData();
    $email             = $("#email").val();
    showLoading();
    $.ajax({
      url    : "{{Route('landing.store')}}",
      method : "POST",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      data   : {
        "about" : $about,
        "blackquote_content" : $blackquote,
        "blackquote_author" : $blackquote_author,
        "blackquote_title" : $blackquote_title,
        "contact" : $contact,
        "email" : $email,
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          miniNotif("success",res.pesan);
        } else
          miniNotif("error",res.pesan);
      },
      error  : function() {
        hideLoading();
        miniNotif("error","Tidak dapat terhubung ke server!");
      }
    })
  }
</script>
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/editors/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
@endsection