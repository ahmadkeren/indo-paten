@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Daftar Customer</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Panel Management</a></li>
        <li class="breadcrumb-item"><a href="#">Data Customer</a></li>
        <li class="breadcrumb-item active">Daftar Customer</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Customer</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <table class="table table-striped table-bordered column-visibility" id="my_table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Tarif</th>
                  <th>Harga</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $index => $val)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$val->jenis_permohonan->jenis_permohonan}} @if($val->pemohon!=null && $val->pemohon!="") ({{$val->pemohon}}) @endif</td>
                  <td>
                    <input type="number" name="" min="0" max="99999999999999" maxlength="11" value="{{$val->harga}}" class="form-control" step="100000" id="harga-{{$val->id_set_harga}}">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-info" onclick="update_harga('{{$val->id_set_harga}}')"><i class="la la-edit"></i> Ubah</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  function update_harga($id_set_harga) {
    $harga = $("#harga-"+$id_set_harga).val();
    showLoading();
    $.ajax({
      url    : "{{Route('set_harga')}}/"+$id_set_harga,
      method : "POST",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      data   : {
        "harga" : $harga,
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          miniNotif("success",res.pesan);
        } else
          miniNotif("error",res.pesan);
      },
      error  : function() {
        hideLoading();
        miniNotif("error","Tidak dapat terhubung ke server!");
      }
    })
  }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Drag And Drop -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/ui/dragula.min.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Drag And Drop -->
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/dragula.min.js')}}" type="text/javascript"></script>
@endsection