/*
 Navicat Premium Data Transfer

 Source Server         : Database Local
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : halo_paten

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 21/12/2019 11:29:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `api` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'URL API',
  `routeName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Route dari API',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Deskripsi API',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for customer_message
-- ----------------------------
DROP TABLE IF EXISTS `customer_message`;
CREATE TABLE `customer_message`  (
  `id_chat` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) UNSIGNED NOT NULL,
  `is_answer` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `has_read` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id_chat`) USING BTREE,
  INDEX `id_customer`(`id_customer`) USING BTREE,
  CONSTRAINT `customer_message_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for data_berkas
-- ----------------------------
DROP TABLE IF EXISTS `data_berkas`;
CREATE TABLE `data_berkas`  (
  `id_data_berkas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_permohonan` int(11) UNSIGNED NOT NULL,
  `id_syarat_berkas` int(11) UNSIGNED NOT NULL,
  `berkas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_data_berkas`) USING BTREE,
  INDEX `id_syarat_berkas`(`id_syarat_berkas`) USING BTREE,
  INDEX `id_permohonan`(`id_permohonan`) USING BTREE,
  CONSTRAINT `data_berkas_ibfk_1` FOREIGN KEY (`id_syarat_berkas`) REFERENCES `syarat_berkas` (`id_syarat_berkas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_berkas_ibfk_2` FOREIGN KEY (`id_permohonan`) REFERENCES `permohonan` (`id_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for data_kelas
-- ----------------------------
DROP TABLE IF EXISTS `data_kelas`;
CREATE TABLE `data_kelas`  (
  `id_data_kelas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_permohonan` int(11) UNSIGNED NOT NULL,
  `id_kelas` int(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_data_kelas`) USING BTREE,
  INDEX `id_permohonan`(`id_permohonan`) USING BTREE,
  INDEX `id_kelas`(`id_kelas`) USING BTREE,
  CONSTRAINT `data_kelas_ibfk_1` FOREIGN KEY (`id_permohonan`) REFERENCES `permohonan` (`id_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_kelas_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas_master` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for data_merek
-- ----------------------------
DROP TABLE IF EXISTS `data_merek`;
CREATE TABLE `data_merek`  (
  `id_data_merek` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_permohonan` int(11) UNSIGNED NOT NULL,
  `id_tipe_permohonan` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_tipe_merek` int(11) UNSIGNED NULL DEFAULT NULL,
  `label_merek` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `merek` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_label_merek` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `unsur_warna_label_merek` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_data_merek`) USING BTREE,
  INDEX `id_permohonan`(`id_permohonan`) USING BTREE,
  INDEX `id_tipe_merk`(`id_tipe_merek`) USING BTREE,
  INDEX `id_tipe_permohonan`(`id_tipe_permohonan`) USING BTREE,
  CONSTRAINT `data_merek_ibfk_1` FOREIGN KEY (`id_permohonan`) REFERENCES `permohonan` (`id_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_merek_ibfk_2` FOREIGN KEY (`id_tipe_merek`) REFERENCES `tipe_merek` (`id_tipe_merek`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_merek_ibfk_3` FOREIGN KEY (`id_tipe_permohonan`) REFERENCES `tipe_permohonan` (`id_tipe_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for data_pemohon
-- ----------------------------
DROP TABLE IF EXISTS `data_pemohon`;
CREATE TABLE `data_pemohon`  (
  `id_data_permohon` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_permohonan` int(11) UNSIGNED NOT NULL,
  `no_ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_jenis_pemohon` int(11) UNSIGNED NOT NULL,
  `id_kewarganegaraan` mediumint(8) UNSIGNED NULL DEFAULT NULL,
  `id_kota` mediumint(8) UNSIGNED NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kode_pos` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_whatsapp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_kota_other` mediumint(8) UNSIGNED NULL DEFAULT NULL,
  `alamat_other` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kode_pos_other` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp_other` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_other` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_data_permohon`) USING BTREE,
  INDEX `id_jenis_pemohon`(`id_jenis_pemohon`) USING BTREE,
  INDEX `id_permohonan`(`id_permohonan`) USING BTREE,
  INDEX `id_kota`(`id_kota`) USING BTREE,
  INDEX `id_kota_other`(`id_kota_other`) USING BTREE,
  INDEX `id_kewarganegaraan`(`id_kewarganegaraan`) USING BTREE,
  CONSTRAINT `data_pemohon_ibfk_1` FOREIGN KEY (`id_jenis_pemohon`) REFERENCES `jenis_pemohon` (`id_jenis_pemohon`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_pemohon_ibfk_2` FOREIGN KEY (`id_permohonan`) REFERENCES `permohonan` (`id_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_pemohon_ibfk_3` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_pemohon_ibfk_4` FOREIGN KEY (`id_kota_other`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_pemohon_ibfk_5` FOREIGN KEY (`id_kewarganegaraan`) REFERENCES `negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for data_pemohon_lainnya
-- ----------------------------
DROP TABLE IF EXISTS `data_pemohon_lainnya`;
CREATE TABLE `data_pemohon_lainnya`  (
  `id_pemohon_lainnya` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_permohonan` int(11) UNSIGNED NOT NULL,
  `no_ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_pemohon_lainnya`) USING BTREE,
  INDEX `id_permohonan`(`id_permohonan`) USING BTREE,
  CONSTRAINT `data_pemohon_lainnya_ibfk_1` FOREIGN KEY (`id_permohonan`) REFERENCES `permohonan` (`id_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jenis_pemohon
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pemohon`;
CREATE TABLE `jenis_pemohon`  (
  `id_jenis_pemohon` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jenis_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `harga_merek_baru` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Harga permohoanan merek baru',
  PRIMARY KEY (`id_jenis_pemohon`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jenis_permohonan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_permohonan`;
CREATE TABLE `jenis_permohonan`  (
  `id_jenis_permohonan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jenis_permohonan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_jenis_permohonan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kelas_master
-- ----------------------------
DROP TABLE IF EXISTS `kelas_master`;
CREATE TABLE `kelas_master`  (
  `id_kelas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kelas` tinyint(2) NULL DEFAULT NULL,
  `deskripsi_id` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `deskripsi_en` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `jenis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kelas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59091 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `state_id` mediumint(8) UNSIGNED NOT NULL,
  `state_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country_id` mediumint(8) UNSIGNED NOT NULL,
  `country_code` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `latitude` decimal(10, 8) NOT NULL,
  `longitude` decimal(11, 8) NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT '2014-01-01 02:31:01',
  `updated_on` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `flag` tinyint(1) NOT NULL DEFAULT 1,
  `wikiDataId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cities_test_ibfk_1`(`state_id`) USING BTREE,
  INDEX `cities_test_ibfk_2`(`country_id`) USING BTREE,
  CONSTRAINT `kota_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `provinsi` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `kota_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `negara` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 141852 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kritik_saran
-- ----------------------------
DROP TABLE IF EXISTS `kritik_saran`;
CREATE TABLE `kritik_saran`  (
  `id_kritik_saran` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_kritik_saran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for landing
-- ----------------------------
DROP TABLE IF EXISTS `landing`;
CREATE TABLE `landing`  (
  `id_landing` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `about` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Detail about app',
  `blackquote_content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Konten/isi dari balckquote',
  `blackquote_author` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Author balckquote, eg: Mr. John Gilbert',
  `blackquote_title` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Title dari author blackquote, eg: CEO',
  `contact` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Kontan/isi kontak app',
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Email dari CS app',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp kapan record dibuat',
  `updated_at` timestamp(0) NOT NULL COMMENT 'Timestamp kapan record diupdate',
  `created_by` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Footprinting dari pembuat konten',
  `updated_by` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Footprinting dari pengupdate konten',
  PRIMARY KEY (`id_landing`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `updated_by`(`updated_by`) USING BTREE,
  CONSTRAINT `landing_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `landing_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for login_logs
-- ----------------------------
DROP TABLE IF EXISTS `login_logs`;
CREATE TABLE `login_logs`  (
  `id_log_login` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int(11) UNSIGNED NOT NULL,
  `login_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_log_login`) USING BTREE,
  INDEX `id_user`(`id_user`) USING BTREE,
  CONSTRAINT `login_logs_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_child
-- ----------------------------
DROP TABLE IF EXISTS `menu_child`;
CREATE TABLE `menu_child`  (
  `id_menu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `parent_id` int(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Foreign key',
  `menu_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Nama menu',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL/link menu',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Deskripsi menu',
  `route_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Nama route menu',
  `visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Status visiblity dalam menu. IF FALSE THEN menu tidak ditampakan ke User',
  PRIMARY KEY (`id_menu`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `menu_child_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu_parent` (`id_parent`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_header
-- ----------------------------
DROP TABLE IF EXISTS `menu_header`;
CREATE TABLE `menu_header`  (
  `id_header` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `header_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nama header menu',
  PRIMARY KEY (`id_header`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_parent
-- ----------------------------
DROP TABLE IF EXISTS `menu_parent`;
CREATE TABLE `menu_parent`  (
  `id_parent` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `id_header` int(11) UNSIGNED NOT NULL COMMENT 'Foreign key',
  `parent_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nama parent menu',
  `icon` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Icon menu',
  PRIMARY KEY (`id_parent`, `id_header`) USING BTREE,
  INDEX `id_header`(`id_header`) USING BTREE,
  CONSTRAINT `menu_parent_ibfk_1` FOREIGN KEY (`id_header`) REFERENCES `menu_header` (`id_header`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for negara
-- ----------------------------
DROP TABLE IF EXISTS `negara`;
CREATE TABLE `negara`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `iso3` char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `iso2` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phonecode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `capital` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `flag` tinyint(1) NOT NULL DEFAULT 1,
  `wikiDataId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 248 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for permohonan
-- ----------------------------
DROP TABLE IF EXISTS `permohonan`;
CREATE TABLE `permohonan`  (
  `id_permohonan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pemohon` int(11) UNSIGNED NOT NULL,
  `jumlah_kelas` int(2) UNSIGNED NULL DEFAULT 1,
  `unknown_class` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Deskripsi untuk unknown class (customer yang menggunakan opsi \'tidak tau class\'). Note: null == known class',
  `bukti_pembayaran` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'URL File',
  `pembayaran_dikonfirmasi` tinyint(1) NULL DEFAULT 0 COMMENT 'Status Pembayaran',
  `tanggal_pengajuan` datetime(0) NULL DEFAULT NULL,
  `nomor_permohonan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Diisi oleh Admin',
  `id_status_permohonan` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Status permohonan ke Dirjen',
  `tanda_terima` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'URL File',
  `surat_pernyataan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'URL File',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `payment_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Id Invoice dari Penyedia Payment Gateway',
  `invoice_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Link External Invoice dari Penyedia Payment Gateway',
  `expiry_date` datetime(0) NULL DEFAULT NULL COMMENT 'Tanggal expired Invoice dari Penyedia Payment Gateway',
  `is_revisi` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'TRUE jika ini adalah permohonan revisi',
  `is_processed` tinyint(1) NOT NULL DEFAULT 0,
  `processed_by` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id_permohonan`) USING BTREE,
  INDEX `id_pemohon`(`id_pemohon`) USING BTREE,
  INDEX `id_status_permohonan`(`id_status_permohonan`) USING BTREE,
  INDEX `processed_by`(`processed_by`) USING BTREE,
  CONSTRAINT `permohonan_ibfk_2` FOREIGN KEY (`id_status_permohonan`) REFERENCES `status_permohonan` (`id_status_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permohonan_ibfk_3` FOREIGN KEY (`id_pemohon`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permohonan_ibfk_4` FOREIGN KEY (`processed_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS `provinsi`;
CREATE TABLE `provinsi`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country_id` mediumint(8) UNSIGNED NOT NULL,
  `country_code` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fips_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `iso2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `flag` tinyint(1) NOT NULL DEFAULT 1,
  `wikiDataId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `country_region`(`country_id`) USING BTREE,
  CONSTRAINT `country_region_final` FOREIGN KEY (`country_id`) REFERENCES `negara` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4852 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Nama Role',
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Deskripsi Role',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for role_api
-- ----------------------------
DROP TABLE IF EXISTS `role_api`;
CREATE TABLE `role_api`  (
  `id_role` int(11) UNSIGNED NOT NULL COMMENT 'Primary key',
  `id_api` int(11) UNSIGNED NOT NULL COMMENT 'Foreign key',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp kapan record dibuat',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT 'Timestamp kapan record diupdate',
  PRIMARY KEY (`id_role`, `id_api`) USING BTREE,
  INDEX `id_api`(`id_api`) USING BTREE,
  CONSTRAINT `role_api_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_api_ibfk_2` FOREIGN KEY (`id_api`) REFERENCES `api` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `id_role` int(10) UNSIGNED NOT NULL COMMENT 'Primary key',
  `id_menu` int(10) UNSIGNED NOT NULL COMMENT 'Foreign key',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp kapan record dibuat',
  `updated_at` timestamp(0) NOT NULL COMMENT 'Timestamp kapan record diupdate',
  PRIMARY KEY (`id_role`, `id_menu`) USING BTREE,
  INDEX `id_menu`(`id_menu`) USING BTREE,
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menu_child` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for set_harga
-- ----------------------------
DROP TABLE IF EXISTS `set_harga`;
CREATE TABLE `set_harga`  (
  `id_set_harga` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_jenis_permohonan` int(11) UNSIGNED NOT NULL,
  `pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga` int(10) NOT NULL,
  `satuan_harga` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_jenis_ref` int(11) UNSIGNED NULL DEFAULT NULL,
  `field_target_ref` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_set_harga`) USING BTREE,
  INDEX `id_jenis_permohonan`(`id_jenis_permohonan`) USING BTREE,
  INDEX `id_jenis_ref`(`id_jenis_ref`) USING BTREE,
  CONSTRAINT `set_harga_ibfk_1` FOREIGN KEY (`id_jenis_permohonan`) REFERENCES `jenis_permohonan` (`id_jenis_permohonan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `set_harga_ibfk_2` FOREIGN KEY (`id_jenis_ref`) REFERENCES `jenis_pemohon` (`id_jenis_pemohon`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for status_permohonan
-- ----------------------------
DROP TABLE IF EXISTS `status_permohonan`;
CREATE TABLE `status_permohonan`  (
  `id_status_permohonan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `selectable` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id_status_permohonan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for syarat_berkas
-- ----------------------------
DROP TABLE IF EXISTS `syarat_berkas`;
CREATE TABLE `syarat_berkas`  (
  `id_syarat_berkas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_jenis_pemohon` int(11) UNSIGNED NOT NULL,
  `jenis_berkas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_format` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_syarat_berkas`) USING BTREE,
  INDEX `id_jenis_pemohon`(`id_jenis_pemohon`) USING BTREE,
  CONSTRAINT `syarat_berkas_ibfk_1` FOREIGN KEY (`id_jenis_pemohon`) REFERENCES `jenis_pemohon` (`id_jenis_pemohon`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tipe_merek
-- ----------------------------
DROP TABLE IF EXISTS `tipe_merek`;
CREATE TABLE `tipe_merek`  (
  `id_tipe_merek` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merek` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tipe_merek`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tipe_permohonan
-- ----------------------------
DROP TABLE IF EXISTS `tipe_permohonan`;
CREATE TABLE `tipe_permohonan`  (
  `id_tipe_permohonan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permohonan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tipe_permohonan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id_user` int(11) UNSIGNED NOT NULL COMMENT 'Primary key',
  `id_role` int(11) UNSIGNED NOT NULL COMMENT 'Foreign key',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp kapan record dibuat',
  `updated_at` timestamp(0) NOT NULL COMMENT 'Timestamp kapan record diupdate',
  PRIMARY KEY (`id_user`, `id_role`) USING BTREE,
  INDEX `id_role`(`id_role`) USING BTREE,
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Nama lengkap dari pengguna',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Username pengguna',
  `usertype` enum('user','admin','karyawan') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'user',
  `credit` tinyint(3) UNSIGNED NULL DEFAULT 0,
  `ponsel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Password pengguna (hash)',
  `photo` blob NULL COMMENT 'Foto pengguna',
  `last_seen` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT 'Timestamp last seen pengguna',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp kapan record dibuat',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT 'Timestamp kapan record diupdate',
  `complete_guide` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Complete guide',
  `remember_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Remember token',
  `removable` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'IF True >> User dapat dihapus',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
