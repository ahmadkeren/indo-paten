<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*======================= Public Access ============================ */
Route::get('/', 'LandingController@index')->name('landing');
Route::get('/', function () {
    return redirect(route('page')."/home");
})->name('root');

// Bagian Page
Route::get('/page/{location?}','PageController@content_page')->name('page');
Route::any('/service/{method?}/{parameter?}','ServiceController@index')->name('service');
Route::resource('/user/permohonan-user', 'UserPermohonanController');
Route::resource('/user/permohonan-user-revisi', 'UserPermohonanRevisiController');
Route::get('/detail/invoice/{id_permohonan?}', 'DetailController@invoice')->name('invoice');
Route::get('/cooming_soon/service', 'CommingSoonController@service')->name('comming_service');

// Bagian auth
Auth::routes();

// Payment System
Route::get('/payment/bayar/{id?}','PaymentController@bayarMerekBaru')->name('bayar_merek_baru');


Route::group(['middleware' => 'authGuard'], function () {
// Bagian Berotoritas untuk menu users
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/user/permohonan-user/update', 'UserPermohonanController@updatePermohonan');
Route::post('/user/customer-message/send', 'CustomerMessageController@sendMessage')->name("send_cusmes_from_cst");
Route::post('/user/customer-message/send/admin/{id?}', 'CustomerMessageController@sendMessageAdmin')->name("send_cusmes_from_admin");
Route::get('/user/{page?}', 'UserPageController@content_page')->name('user_page');
Route::get('/admin/{page?}', 'AdminPageController@content_page')->name('admin_page');
Route::resource('/user_profile', 'UserController');
Route::any('/admin/handle-permohonan-baru/{handle?}/{param?}', 'HandlePermohonanBaruController@handle_method')->name("handle_permohonan_baru");
});


// Menu Admin dan karyawan
Route::group(['middleware' => 'authGuard'], function () {
Route::group(['middleware' => 'authMenu'], function () {
/*=========================== Bagian Menu =====================================*/
Route::get('/dashboard', 'GUI\DashboardController@index')->name('dashboard');
Route::get('/profil_edit', 'GUI\ProfilController@index')->name('profil_edit');
Route::get('/profil_password', 'GUI\ProfilController@password')->name('profil_password');
Route::get('/set-user_role', 'GUI\PengaturanController@user_role')->name('set_user_role');
Route::get('/set-role_menu', 'GUI\PengaturanController@role_menu')->name('set_role_menu');
Route::get('/set-role_api', 'GUI\PengaturanController@role_api')->name('set_role_api');
Route::get('/set_landing', 'GUI\PengaturanController@landing_set')->name('set_landing');
Route::get('/set-harga', 'GUI\PengaturanController@konfig_harga')->name('set-harga');
Route::post('/logout','Auth\LoginController@logout')->name('logout');
/*=========================== Bagian Menu =====================================*/
Route::get('/user_tambah', 'GUI\UserController@tambah')->name('user_tambah');
Route::get('/user_list', 'GUI\UserController@daftar')->name('user_list');
Route::get('/user_edit/{id?}', 'GUI\UserController@edit')->name('user_edit');
Route::get('/karyawan_list', 'GUI\UserController@daftar_karyawan')->name('karyawan_list');
Route::get('/karyawan_tambah', 'GUI\UserController@tambah_karyawan')->name('karyawan_tambah');
Route::get('/karyawan_edit/{id?}', 'GUI\UserController@edit_karyawan')->name('karyawan_edit');
Route::get('/permohonan_pending', 'GUI\PermohonanController@permohonan_pending')->name('permohonan_pending');
Route::get('/permohonan_masuk', 'GUI\PermohonanController@permohonan_masuk')->name('permohonan_masuk');
Route::get('/permohonan_diproses', 'GUI\PermohonanController@permohonan_diproses')->name('permohonan_diproses');
Route::get('/permohonan_berhasil', 'GUI\PermohonanController@permohonan_berhasil')->name('permohonan_berhasil');
Route::get('/permohonan_dibatalkan', 'GUI\PermohonanController@permohonan_dibatalkan')->name('permohonan_dibatalkan');
Route::get('/permohonan_revisi', 'GUI\PermohonanController@permohonan_revisi')->name('permohonan_revisi');
Route::get('/respon_kontak', 'GUI\KotakMasukController@respon_kontak')->name('respon_kontak');
Route::get('/customer_message/{id?}', 'GUI\KotakMasukController@customer_message')->name('customer_message');
Route::get('/logs_login', 'GUI\LogsController@login')->name('logs_login');
// ==============================================================================
});
// Batas auth menu
});

/*=========================== Bagian API =====================================*/
Route::get('/api/data_master/user','API\Service\UserController@getUser')->name('get_user');
Route::get('/api/data_master/karyawan','API\Service\UserController@getKaryawan')->name('get_karyawan');
Route::get('/api/data_master/customer','API\Service\UserController@getCustomer')->name('get_customer');
Route::post('/api/set_harga/{id?}','API\Service\HargaController@setHarga')->name('set_harga');
Route::group(['middleware' => 'authToken'], function () {
// ============================ Middleware token
Route::group(['middleware' => 'authRole'], function () {
// ============================ Middleware authRole

Route::resource(
	'/api/landing',
	'API\Master\LandingController',
	['only' => ['store']]
);

Route::resource(
	'/api/user',
	'API\User\UserController',
	['only' => ['index','show','store']]
);

Route::resource(
	'/api/pengguna',
	'API\Master\UserController',
	['only' => ['store','destroy','update']]
);

Route::resource(
	'/api/role',
	'API\Role\RoleController',
	['only' => ['index','show','store']]
);

Route::resource(
	'/api/role_menu',
	'API\Role\RoleMenuController',
	['only' => ['index','show','store']]
);

Route::resource(
	'/api/role_api',
	'API\Role\RoleAPIController',
	['only' => ['index','show','store']]
);
// ============================ End of authRole middleware
});
// ============================ End of token middleware
});

// debuging zone
Route::get('/payment','PaymentController@check');
// debuging zone

// ====================== Another Open Access ========================//
Route::get('/api/ping',function(){
	return [
		"success" => true,
	];
});

Route::get('/api/global_function',
	function() {return "Global Function";}
)->name('global_function');

Route::get('/api/global_function/encrypt/{id?}','GlobalController@encrypt');
// ====================== Another Open Access ========================//

// ====================== Error Classification ========================//
Route::get('/errors/access', 'GUI\ErrorController@access_danied')->name('access_danied');
Route::get('/errors/null', 'GUI\ErrorController@access_invalid')->name('access_invalid');